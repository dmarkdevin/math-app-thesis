<?php
require_once '../global.php';

$page['title'] = 'Add Section';

authorize('teacher');

require_once DOCUMENT_ROOT.'system/pages/teacher/header.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/add_section.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/footer.php';