<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/quizResults.php'; 

$page['title'] = 'Quiz Results';
$label = 'results';

authorize('teacher'); 

$database = new Database();
$db = $database->getConnection(); 

$quizResults = new QuizResults($db);

$quizResults->teacher_reg_id = $_SESSION['mathapp']['login']['teacher'];

$stmt = $quizResults->get();  
$dataCount = $stmt->rowCount();

$dataArray = array();
$dataArray["body"] = array();
$dataArray["itemCount"] = $dataCount;

if ($dataCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
 

        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "right_answer" => $right_answer, 
            "wrong_answer" => $wrong_answer, 
            "unanswered" => $unanswered, 
            "username" => $username,
            "first_name" => $first_name,
            "last_name" => $last_name,
            "module_title" => $module_title,
            "lesson_title" => $lesson_title,
            "section_name" => $section_name,
            "date_added" => $date_added,
            "category_name" => $category_name 
            
            
        );
        
        array_push($dataArray["body"], $e);
    } 
}

$page['datatable'] = true;

require_once DOCUMENT_ROOT.'system/pages/teacher/header.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/quiz_results.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/footer.php';
