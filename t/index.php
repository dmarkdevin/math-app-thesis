<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/modules.php'; 
require_once DOCUMENT_ROOT.'system/classes/lessons.php'; 
require_once DOCUMENT_ROOT.'system/classes/sections.php'; 

$page['title'] = 'Dashboard';

authorize('teacher');


$database = new Database();
$db = $database->getConnection(); 

$module = new Modules($db);
$module->teacher_reg_id = $_SESSION['mathapp']['login']['teacher'];
$stmt = $module->get();  
$moduleCount = $stmt->rowCount();

$lesson = new Lessons($db);
$lesson->teacher_reg_id = $_SESSION['mathapp']['login']['teacher'];
$stmt = $lesson->get();  
$lessonCount = $stmt->rowCount();

$section = new Sections($db);
$section->teacher_reg_id = $_SESSION['mathapp']['login']['teacher'];
$stmt = $section->get();  
$sectionCount = $stmt->rowCount();


require_once DOCUMENT_ROOT . 'system/pages/teacher/header.php';
require_once DOCUMENT_ROOT . 'system/pages/teacher/index.php';
require_once DOCUMENT_ROOT . 'system/pages/teacher/footer.php';