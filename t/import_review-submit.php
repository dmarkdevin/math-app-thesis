<?php
ob_start();
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/pupils.php';

authorize('teacher'); 

$label = 'import_pupil';
$main_page = $label.'s.php';


if (!isset($_SESSION['mathapp']['import-csv']['data2'])) {
    $_SESSION['mathapp']['error'] = 'invalid request'; 
    header('location: '.$main_page);
    exit;
} 

if (!isset($_SESSION['mathapp']['import-csv']['section_id'])) {
    $_SESSION['mathapp']['error'] = 'invalid request 101'; 
    header('location: '.$main_page);
    exit;
} 

if (!isset($_SESSION['mathapp']['import-csv']['status'])) {
    $_SESSION['mathapp']['error'] = 'invalid request 102'; 
    header('location: '.$main_page);
    exit;
} 

$dataArray['body'] = $_SESSION['mathapp']['import-csv']['data2'];
unset($_SESSION['mathapp']['import-csv']['data2']);

$database = new Database();
$db = $database->getConnection();


$$label = new Pupils($db);


foreach($dataArray['body'] as $data){

    $student_id = $data[2];
    $student_id2 = $data[2];
    $section_id = $_SESSION['mathapp']['import-csv']['section_id'];
    $status = $_SESSION['mathapp']['import-csv']['status'];

    $$label->student_id = $student_id;
    $$label->section_id = $section_id;

    $$label->getByStudentIDSection();

    if ($$label->reg_id == null) { 

        $create = new Pupils($db); 

        $create->first_name = $data[0]; 
        $create->last_name = $data[1];

       

        if(!empty($data[3])){

            $gender = $data[3];
            $gender = $gender[0];
            $gender = strtoupper($gender);

            $gender = ($gender == 'M' OR $gender == 'F') ? $gender : '';

        }else{

            $gender = '';

        } 

        if($gender=='M'){
            $avatar = rand(1,3);
        }else if($gender=='F'){
            $avatar = rand(4,6);
        }else{
            $avatar = '0';
        }

        $create->gender = $gender;
        $create->avatar = $avatar;

        $create->student_id = $student_id;
        $create->username = $student_id; 
        $create->password = rand(10,99); 
        $create->status = $status;   
        $create->section_id = $section_id;   
        $create->reg_type = 'teacher';  
        $create->teacher_reg_id = $_SESSION['mathapp']['login']['teacher']; 
       
        if($create->create()){

            pr($create);
            echo $create->student_id;
            echo '<br>';

        }

    }  

}
 
if(isset($_SESSION['mathapp']['error'])){
    unset($_SESSION['mathapp']['error']);
}

header('location: sections.php');
exit;
