<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/modules.php'; 


authorize('teacher');

$label = 'module';
$main_page = $label.'s.php';

if(empty($_POST['id'])){
    header('location: '.$main_page);
    exit;
} 

$decryptedID = encrypt_decrypt('decrypt', $_POST['id']);

if (empty($_POST['module_title'])) {
    $_SESSION['mathapp']['error'] = 'Please enter a module_title'; 
    header('location: '.FILE_BASENAME.'?id='.$decryptedID);
    exit;
}

$database = new Database();
$db = $database->getConnection();

$$label = new Modules($db);
$$label->reg_id = $decryptedID;  

$$label->getSingle();

if($$label->reg_id == null){
    header('location: '.$main_page);
    exit;
}

if($$label->teacher_reg_id!=$_SESSION['mathapp']['login']['teacher']){
    header('location:'.$main_page);
    exit;
}

if(
    $$label->category_id == $_POST['category_id'] AND
    $$label->module_title == $_POST['module_title']  AND
    $$label->status == $_POST['status']  
    ){
        
        $_SESSION['mathapp']['error'] = 'No changes have been made';
        header('location: '.FILE_BASENAME.'?id='.$_POST['id']);
        exit;
    }



$check = new Modules($db);

$check->category_id = $_POST['category_id'];
$check->module_title = $_POST['module_title'];
$check->teacher_reg_id = $_SESSION['mathapp']['login']['teacher'];

$check->getByTitleCategoryIDTeacherRegID();

if ($check->module_title != null) { 
    
    if( $$label->status == $_POST['status'] ){
        $_SESSION['mathapp']['error'] = 'Module Title Already Exists ';
        header('location: '.FILE_BASENAME.'?id='.$_POST['id']);
        exit; 
    }
  
 
} 


$update = new Modules($db);

$update->module_title = strtoupper($_POST['module_title']); 
$update->category_id = $_POST['category_id']; 
$update->status = $_POST['status'];
$update->reg_id = $decryptedID;  

if($update->update()){
    $_SESSION['mathapp']['success'] = $label.' updated successfully.';
} else {
    $_SESSION['mathapp']['error'] = $label.' could not be updated.';
}


$logs = new ActivityLogs($db);
$logs->account_type = 'teacher'; 
$logs->account_id = $_SESSION['mathapp']['login']['teacher'];   
$logs->activity = 'teacher edit module'; 
$logs->description = REQUEST_URI;
$logs->ip_address = '';
$logs->create(); 

header('location: '.FILE_BASENAME.'?id='.$_POST['id']);