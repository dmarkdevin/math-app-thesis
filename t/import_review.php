<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/quizResults.php'; 

$page['title'] = 'Import CSV - Review';
$label = 'results';


authorize('teacher'); 
 

$database = new Database();
$db = $database->getConnection(); 

$quizResults = new QuizResults($db);

$quizResults->teacher_reg_id = $_SESSION['mathapp']['login']['teacher'];

// $stmt = $quizResults->get();  
// $dataCount = $stmt->rowCount();

$dataArray = array();
$dataArray["body"] = array();
$dataArray["itemCount"] = $dataCount;

// if ($dataCount > 0) { 
//     while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
 

//         extract($row);   
//         $e = array(
//             "reg_id" => $reg_id,
//             "right_answer" => $right_answer, 
//             "wrong_answer" => $wrong_answer, 
//             "unanswered" => $unanswered, 
//             "username" => $username,
//             "module_title" => $module_title,
//             "lesson_title" => $lesson_title,
//             "section_name" => $section_name,
//             "date_added" => $date_added,
//             "category_name" => $category_name  
//         );
        
//         array_push($dataArray["body"], $e);
//     } 
// }

if(isset($_SESSION['mathapp']['import-csv']['data'])){
    $dataArray['body'] = $_SESSION['mathapp']['import-csv']['data'];
    $_SESSION['mathapp']['import-csv']['data2'] = $_SESSION['mathapp']['import-csv']['data'];
    unset($_SESSION['mathapp']['import-csv']['data']);
}else{
    header('location: import_pupils.php');
    exit;
}
 

require_once DOCUMENT_ROOT.'system/pages/teacher/header.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/import_review.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/footer.php';
