<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/modules.php'; 

authorize('teacher');

$label = 'module';
$main_page = $label.'s.php';


if(empty($_GET['id'])){
    header('location:'.$main_page);
    exit;
}

$decryptedID = encrypt_decrypt('decrypt', $_GET['id']);

$database = new Database();
$db = $database->getConnection();

$$label = new Modules($db);
$$label->reg_id = $decryptedID;  

$$label->getSingle();

if($$label->reg_id == null){
    header('location: '.$main_page);
    exit;
}

if($$label->teacher_reg_id!=$_SESSION['mathapp']['login']['teacher']){
    header('location:'.$main_page);
    exit;
} 

if($$label->delete()){ 
    $_SESSION['mathapp']['success'] = $label.' deleted successfully.';
} else { 
    $_SESSION['mathapp']['error'] = $label.' could not be deleted.';
}

$logs = new ActivityLogs($db);
$logs->account_type = 'teacher'; 
$logs->account_id = $_SESSION['mathapp']['login']['teacher'];   
$logs->activity = 'teacher delete module'; 
$logs->description = REQUEST_URI;
$logs->ip_address = '';
$logs->create(); 

header('location: '.$main_page);