<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/sections.php'; 

$page['title'] = 'Import CSV - Pupil Data';

authorize('teacher');

$database = new Database();
$db = $database->getConnection(); 

$section = new Sections($db); 
$section->teacher_reg_id = $_SESSION['mathapp']['login']['teacher'];
$stmt = $section->getActive();  
$sectionCount = $stmt->rowCount();

$sectionArr = array();
$sectionArr["body"] = array();
$sectionArr["itemCount"] = $sectionCount;

if ($sectionCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "section_name" => $section_name
        );
        
        array_push($sectionArr["body"], $e);
    } 
}

if(isset($_SESSION['mathapp']['import-csv']['data'])){
    unset($_SESSION['mathapp']['import-csv']['data']);
}  

if(isset($_SESSION['mathapp']['import-csv']['section_id'])){
    unset($_SESSION['mathapp']['import-csv']['section_id']);
}  

if(isset($_SESSION['mathapp']['import-csv']['status'])){
    unset($_SESSION['mathapp']['import-csv']['status']);
}  
 
require_once DOCUMENT_ROOT.'system/pages/teacher/header.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/import_pupils.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/footer.php';