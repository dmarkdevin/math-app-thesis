<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/category.php'; 

$page['title'] = 'Add Module';

authorize('teacher');

$database = new Database();
$db = $database->getConnection(); 

$category = new Category($db); 
 
$stmt = $category->get();  
$categoryCount = $stmt->rowCount();

$categoryArr = array();
$categoryArr["body"] = array();
$categoryArr["itemCount"] = $categoryCount;

if ($categoryCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "id" => $id,
            "category_name" => $category_name
        );
        
        array_push($categoryArr["body"], $e);
    } 
} 

require_once DOCUMENT_ROOT . 'system/pages/teacher/header.php';
require_once DOCUMENT_ROOT . 'system/pages/teacher/add_module.php';
require_once DOCUMENT_ROOT . 'system/pages/teacher/footer.php';