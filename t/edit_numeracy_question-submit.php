<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/numeracyQuestions.php'; 

authorize('teacher');

$label = 'numeracy_question';
$main_page = $label.'s.php';

if(empty($_POST['id'])){
    header('location: '.$main_page);
    exit;
} 

$decryptedID = encrypt_decrypt('decrypt', $_POST['id']);

if (empty($_POST['number1']) OR empty($_POST['number2'])) {
    $_SESSION['mathapp']['error'] = 'Please enter a question'; 
    header('location: '.FILE_BASENAME.'?id='.$_POST['id']);
    exit;
}

$database = new Database();
$db = $database->getConnection();

$$label = new numeracyQuestions($db);
$$label->reg_id = $decryptedID;  

$$label->getSingle();

if($$label->reg_id == null){
    header('location: '.$main_page);
    exit;
}

if($$label->teacher_reg_id!=$_SESSION['mathapp']['login']['teacher']){
    header('location:'.$main_page);
    exit;
}




$$label->number1 = $_POST['number1']; 
$$label->number2 = $_POST['number2']; 
$$label->answer = $_POST['answer']; 
$$label->operator = $_POST['operator']; 
$$label->numeracy = $_POST['numeracy']; 
$$label->status = $_POST['status']; 
$$label->reg_id = $decryptedID; 

if($$label->update()){
    $_SESSION['mathapp']['success'] = $label.' updated successfully.';
} else {
    $_SESSION['mathapp']['error'] = $label.' could not be updated.';
}

header('location: '.FILE_BASENAME.'?id='.$_POST['id']);