<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php'; 
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/teachers.php'; 
require_once DOCUMENT_ROOT.'system/classes/emailNotification.php'; 
require_once DOCUMENT_ROOT.'assets/vendor/PHPMailer/PHPMailerAutoload.php'; 
 

if(empty($_SESSION['mathapp']['resend']['teacher']))
{
    header('location: '.TEACHER_SIGNUP);
    exit;
} 

$database = new Database(); 
$db = $database->getConnection(); 

$teacher = new Teachers($db);
$teacher->reg_id = isset($_SESSION['mathapp']['resend']['teacher']) ? $_SESSION['mathapp']['resend']['teacher'] : die();  

$teacher->getSingle();



if($teacher->first_name != null){

    if($teacher->status ==1){
        header('location: '.TEACHER_SIGNUP);
        exit;
    }
    

    $verification_link = BASE_URL.'teacher/verify.php?token='.$teacher->reg_id; 
   
    $verification_link = BASE_URL.'t/verify.php?token='.encrypt_decrypt('encrypt',$teacher->reg_id);

	$emailNotification = new emailNotification();
	$emailNotification->token = $verification_link; 

	$emailer = new PHPMailer();
 
	$emailer->IsSMTP();
	$emailer->SMTPAuth  = true;
	$emailer->SMTPSecure = $system_config['email_SMTPSecure']; 
	$emailer->Host       = EMAIL_HOST;
	$emailer->Port       = EMAIL_PORT;
	$emailer->Username   = EMAIL_USERNAME;
	$emailer->Password   = EMAIL_PASSWORD;

	$emailer->From 		= EMAIL_FROM;
	$emailer->FromName  = EMAIL_FROM_NAME;
	$emailer->Subject   = "E-mail Verification - MathApp3.com";
	$emailer->Body      = $emailNotification->emailVerification();
 
	$emailer->IsHTML(true);
	$emailer->AddAddress($teacher->email);
	
	if($emailer->Send()){

		$_SESSION['mathapp']['success'] = 'We have sent an email to '.$teacher->email.' , please click a link to verify your email';
    
		header('location: '.TEACHER_SIGNUP);
		exit;

	}else{
		 
		$_SESSION['mathapp']['error'] = 'ERROR: CANNOT SEND EMAIL USING LOCAL SERVER ' ; 
		header('location: '.TEACHER_SIGNUP);
		exit;

	}  
    
}else{
    header('location: '.TEACHER_SIGNUP);
    exit;
}