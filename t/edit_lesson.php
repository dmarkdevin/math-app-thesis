<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/lessons.php'; 
require_once DOCUMENT_ROOT.'system/classes/modules.php';

$page['title'] = 'Edit lesson';

authorize('teacher');

$label = 'lesson';
$main_page = $label.'s.php';

if(empty($_GET['id'])){
    header('location:'.$main_page);
    exit;
}

$decryptedID = encrypt_decrypt('decrypt', $_GET['id']);

$database = new Database();
$db = $database->getConnection();

$$label = new Lessons($db);
$$label->reg_id = $decryptedID;  

$$label->getSingle();

if($$label->reg_id == null){
    header('location: '.$main_page);
    exit;
}

if($$label->teacher_reg_id!=$_SESSION['mathapp']['login']['teacher']){
    header('location:'.$main_page);
    exit;
} 

$data = array(
    "reg_id" =>  $$label->reg_id,
    "lesson_title" => $$label->lesson_title,
    "lesson_file" => $$label->lesson_file,
    "module_id" => $$label->module_id,
    "status" => $$label->status 
);

$module = new Modules($db);
$module->teacher_reg_id = $_SESSION['mathapp']['login']['teacher'];
$stmt = $module->getActive();  
$moduleCount = $stmt->rowCount();

$moduleArr = array();
$moduleArr["body"] = array();
$moduleArr["itemCount"] = $moduleCount;

if ($moduleCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "module_title" => $module_title,
            "category_name" => $category_name
        );
        
        array_push($moduleArr["body"], $e);
    } 
}
 
require_once DOCUMENT_ROOT.'system/pages/teacher/header.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/add_lesson.php';
require_once DOCUMENT_ROOT.'system/pages/teacher/footer.php';