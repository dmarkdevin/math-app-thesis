<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/schools.php'; 

authorize('admin');

$label = 'school';
$main_page = $label.'s.php';

if(empty($_POST['id'])){
    header('location: '.$main_page);
    exit;
} 

$decryptedID = encrypt_decrypt('decrypt', $_POST['id']);

if (empty($_POST['school_name'])) {
    $_SESSION['mathapp']['error'] = 'Please enter a school_name'; 
    header('location: '.FILE_BASENAME.'?id='.$decryptedID);
    exit;
}

$database = new Database();
$db = $database->getConnection();

$$label = new Schools($db);
$$label->reg_id = $decryptedID;  

$$label->getSingle();

if($$label->reg_id == null){
    header('location: '.$main_page);
    exit;
}

$$label->school_name = $_POST['school_name'];
$$label->getByName();

if($$label->school_name != null AND $$label->school_name != $_POST['school_name']){
    $_SESSION['mathapp']['error'] = 'school_name already exists';
    header('location: '.FILE_BASENAME.'?id='.$_POST['id']);
    exit;
}

$$label->school_name = $_POST['school_name']; 
$$label->school_id = $_POST['school_id'];
$$label->status = $_POST['status'];
$$label->school_address = $_POST['school_address'];   
$$label->language = $_POST['language'];  
$$label->reg_id = $decryptedID;  

if($$label->update()){
    $_SESSION['mathapp']['success'] = 'school updated successfully.';
} else {
    $_SESSION['mathapp']['error'] = 'school could not be updated.';
}

header('location: '.FILE_BASENAME.'?id='.$_POST['id']);