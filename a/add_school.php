<?php
require_once '../global.php';

$page['title'] = 'Add School';

require_once DOCUMENT_ROOT . 'system/pages/admin/header.php';
require_once DOCUMENT_ROOT . 'system/pages/admin/add_school.php';
require_once DOCUMENT_ROOT . 'system/pages/admin/footer.php';