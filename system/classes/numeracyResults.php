<?php
class numeracyResults
{
    // Connection
    private $conn;
    // Table
    private $db_table = "tbl_numeracy_results";
    private $columns =
        [
            'reg_id',
            'addition',
            'subtraction',
            'multiplication',
            'division',

            'right_answer',
            'wrong_answer',
            'unanswered',
            'pupil_id',
            'lesson_id',
            'status',
            'date_added',
            'numeracy' 
        ];
    // Columns
    public $reg_id;
    public $right_answer;
    public $wrong_answer;
    public $status;
    public $unanswered;
    public $pupil_id;
    public $lesson_id;
    public $date_added; 
    public $teacher_reg_id;
    public $numeracy,$n1,$n2;
    public $section_id;

    // Db connection
    public function __construct($db)
    {
        $this->conn = $db;
    }
    public function get()
    {

        // T.teacher_reg_id, T.first_name as Tfirst_name, T.last_name as Tlast_name

        // LEFT JOIN tbl_pupils P ON P.reg_id = R.pupil_id
        // LEFT JOIN tbl_sections S ON P.section_id = S.reg_id  
        // LEFT JOIN tbl_teachers T ON P.teacher_reg_id = T.reg_id
        
        // WHERE T.reg_id = :teacher_reg_id ORDER by date_added DESC


        $newsection_id = '';

        if(!empty($this->section_id)){ 
            $this->section_id=htmlspecialchars(strip_tags($this->section_id)); 
            $newsection_id = 'P.section_id = :section_id AND ';  
        }


        
        $sqlQuery = "
            SELECT R.reg_id, R.right_answer, R.date_added, R.pupil_id, R.addition, R.subtraction, R.multiplication, R.division,
            S.section_name,
            P.first_name, P.last_name
            
            FROM " . $this->db_table . " R
            LEFT JOIN tbl_pupils P ON P.reg_id = R.pupil_id
            LEFT JOIN tbl_sections S ON P.section_id = S.reg_id  
            LEFT JOIN tbl_teachers T ON P.teacher_reg_id = T.reg_id
            WHERE ".$newsection_id." T.reg_id = :teacher_reg_id ORDER by date_added DESC"; 

        $stmt = $this->conn->prepare($sqlQuery); 

        $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id)); 
        $stmt->bindParam(":teacher_reg_id", $this->teacher_reg_id); 

        if(!empty($newsection_id)){
            $stmt->bindParam(":section_id", $this->section_id); 
        }

        $stmt->execute();
        return $stmt;

    }



    public function check(){

     
        $sqlQuery = " 

            SELECT reg_id, numeracy, pupil_id
            
            FROM " . $this->db_table . "
                      
            WHERE numeracy = :numeracy AND pupil_id = :pupil_id ";
 
        $this->numeracy=htmlspecialchars(strip_tags($this->numeracy));
        $this->pupil_id=htmlspecialchars(strip_tags($this->pupil_id));
       
        $stmt = $this->conn->prepare($sqlQuery);

        $stmt->bindParam(":numeracy", $this->numeracy); 
        $stmt->bindParam(":pupil_id", $this->pupil_id); 


        $stmt->execute();

        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $this->reg_id = $dataRow['reg_id'];
        

    } 


}