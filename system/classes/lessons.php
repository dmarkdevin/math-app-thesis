<?php
class Lessons
{
    // Connection
    private $conn;
    // Table
    private $db_table = "tbl_lessons";
    private $columns = 
    [   
        'reg_id',
        'lesson_title',
        'lesson_file',
        'lesson_description',
        'module_id',
        'status', 
        'teacher_reg_id', 
        'date_added' 
    ];
    // Columns
    public $reg_id;
    public $lesson_title;
    public $lesson_file;
    public $lesson_description;
    public $module_id;
    public $status;
    public $date_added;
  
    public $teacher_reg_id;
    // Db connection
    public function __construct($db)
    {
        $this->conn = $db;
    }
    public function get()
    {
        $sqlQuery = "
            SELECT 
                L.reg_id, 
                L.lesson_title, 
                L.lesson_file, 
                L.lesson_description, 
                L.module_id, 
                L.status, 
                L.teacher_reg_id, 
                L.date_added, 
                M.module_title,
                C.category_name
            FROM " . $this->db_table . " L
            LEFT JOIN tbl_modules M ON L.module_id = M.reg_id
            LEFT JOIN tbl_categories C ON M.category_id = C.id
            WHERE L.teacher_reg_id = :teacher_reg_id ";  
        
      
        $stmt = $this->conn->prepare($sqlQuery);

        $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id)); 
        $stmt->bindParam(":teacher_reg_id", $this->teacher_reg_id); 

        $stmt->execute();
        return $stmt;

    }

    public function getSingle(){

        $columns = implode(', ', $this->columns);


        $sqlQuery = "SELECT ".$columns."
                        FROM
                            ". $this->db_table ."
                        WHERE 
                            reg_id = ?  
                        LIMIT 0,1";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));

        $stmt->bindParam(1, $this->reg_id); 
        $stmt->execute();
        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->reg_id = $dataRow['reg_id'];
        $this->lesson_title = $dataRow['lesson_title']; 
        $this->lesson_file = $dataRow['lesson_file']; 
        $this->lesson_description = $dataRow['lesson_description']; 
        $this->status = $dataRow['status']; 
        $this->module_id = $dataRow['module_id']; 
        $this->teacher_reg_id = $dataRow['teacher_reg_id']; 
        
    } 


    public function getByModuleID(){
        $sqlQuery = "
        SELECT 
            L.reg_id, 
            L.lesson_title, 
            L.lesson_file, 
            L.lesson_description, 
            L.module_id, 
            L.status, 
            L.teacher_reg_id, 
            L.date_added, 
            M.module_title
        FROM " . $this->db_table . " L
        LEFT JOIN tbl_modules M ON L.module_id = M.reg_id
        WHERE L.teacher_reg_id = :teacher_reg_id AND L.module_id = :module_id
        AND M.status = 1
        ";  

    $stmt = $this->conn->prepare($sqlQuery);

    $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id)); 
    $this->module_id=htmlspecialchars(strip_tags($this->module_id)); 

    $stmt->bindParam(":module_id", $this->module_id); 
    $stmt->bindParam(":teacher_reg_id", $this->teacher_reg_id); 
   
    $stmt->execute();
    return $stmt;
          
    } 



    public function getByTitle(){

        $columns = implode(', ', $this->columns);


        $sqlQuery = "SELECT ".$columns."
                        FROM
                            ". $this->db_table ."
                        WHERE 
                            lesson_title = ?  
                        LIMIT 0,1";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->lesson_title=htmlspecialchars(strip_tags($this->lesson_title));

        $stmt->bindParam(1, $this->lesson_title); 
        $stmt->execute();
        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->reg_id = $dataRow['reg_id'];
        $this->lesson_title = $dataRow['lesson_title']; 
        $this->lesson_file = $dataRow['lesson_file']; 
        $this->lesson_description = $dataRow['lesson_description']; 
        $this->status = $dataRow['status']; 
        $this->module_id = $dataRow['module_id']; 
        $this->teacher_reg_id = $dataRow['teacher_reg_id']; 
        
    } 




    public function getByTitleModuleIDTeacherRegID(){

        $columns = implode(', ', $this->columns);


        $sqlQuery = "SELECT ".$columns."
                        FROM
                            ". $this->db_table ."
                        WHERE 
                            lesson_title = ?  AND
                            module_id = ? AND
                            teacher_reg_id = ? 
                        LIMIT 0,1";


                        

        $stmt = $this->conn->prepare($sqlQuery);

        $this->lesson_title=htmlspecialchars(strip_tags($this->lesson_title));
        $this->module_id=htmlspecialchars(strip_tags($this->module_id));
        $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id));

        $stmt->bindParam(1, $this->lesson_title); 
        $stmt->bindParam(2, $this->module_id); 
        $stmt->bindParam(3, $this->teacher_reg_id); 
        
        $stmt->execute();
        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->reg_id = $dataRow['reg_id'];
        $this->lesson_title = $dataRow['lesson_title']; 
        $this->lesson_file = $dataRow['lesson_file']; 
        $this->lesson_description = $dataRow['lesson_description']; 
        $this->status = $dataRow['status']; 
        $this->module_id = $dataRow['module_id']; 
        $this->teacher_reg_id = $dataRow['teacher_reg_id']; 
        
    } 




    public function create(){

        $sqlQuery = "INSERT INTO
                    ". $this->db_table ."
                SET
                    lesson_title = :lesson_title,
                    status = :status,
                    module_id = :module_id, 
                 
                    teacher_reg_id = :teacher_reg_id"; 

        $stmt = $this->conn->prepare($sqlQuery);

        $this->lesson_title=htmlspecialchars(strip_tags($this->lesson_title));
        $this->module_id=htmlspecialchars(strip_tags($this->module_id)); 
        
        $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id)); 
      
        $stmt->bindParam(":lesson_title", $this->lesson_title);
        $stmt->bindParam(":status", $this->status);   
        $stmt->bindParam(":module_id", $this->module_id); 
 
        $stmt->bindParam(":teacher_reg_id", $this->teacher_reg_id); 


        if($stmt->execute()){
            return true;
        }
 

        return false;

    }

    public function update(){

        $sqlQuery = "UPDATE
                    ". $this->db_table ."
                SET
                    lesson_title = :lesson_title,
                    status = :status,
                    module_id = :module_id
                WHERE 
                    reg_id = :reg_id";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->lesson_title=htmlspecialchars(strip_tags($this->lesson_title));   
        $this->status=htmlspecialchars(strip_tags($this->status));   
        $this->module_id=htmlspecialchars(strip_tags($this->module_id));   
        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));  
 
        $stmt->bindParam(":lesson_title", $this->lesson_title);   
        $stmt->bindParam(":status", $this->status);   
        $stmt->bindParam(":module_id", $this->module_id);   
        $stmt->bindParam(":reg_id", $this->reg_id);

        if($stmt->execute()){
            return true;
        }

        
        return false;
    }

    function delete(){

        $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE reg_id = ?";
        $stmt = $this->conn->prepare($sqlQuery);
    
        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));

        $stmt->bindParam(1, $this->reg_id);

        if($stmt->execute()){
            return true;
        }

        return false;
    }



}