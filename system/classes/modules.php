<?php
class Modules
{
    // Connection
    private $conn;
    // Table
    private $db_table = "tbl_modules";
    private $columns = 
        [   
            'reg_id',
            'module_title',
            'category_id',
            'status',
            'teacher_reg_id',
            'date_added' 
        ];
    // Columns
    public $reg_id;
    public $module_title;
    public $category_id;
    public $status;
    public $date_added;
  
    public $teacher_reg_id;
    public $category_name;
 
    // Db connection
    public function __construct($db)
    {
        $this->conn = $db;
    }
    public function get()
    {

        $sqlQuery = "
            SELECT M.reg_id, M.module_title, M.category_id, M.status, M.teacher_reg_id, M.date_added, C.category_name
            FROM " . $this->db_table . " M
            LEFT JOIN tbl_categories C ON M.category_id = C.id
            WHERE M.teacher_reg_id = :teacher_reg_id ";  
 
        
        $stmt = $this->conn->prepare($sqlQuery);

        $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id)); 
        $stmt->bindParam(":teacher_reg_id", $this->teacher_reg_id); 

        $stmt->execute();
        return $stmt;
        
    }

    public function getActive(){

        // $columns = implode(', ', $this->columns);

        // $sqlQuery = "SELECT ".$columns."  FROM " . $this->db_table . " WHERE status = 1 AND teacher_reg_id = :teacher_reg_id";
        
        $sqlQuery = "
        SELECT M.reg_id, M.module_title, M.category_id, M.status, M.teacher_reg_id, M.date_added, C.category_name
        FROM " . $this->db_table . " M
        LEFT JOIN tbl_categories C ON M.category_id = C.id
        WHERE M.teacher_reg_id = :teacher_reg_id AND status = 1 ";  
        
        
        $stmt = $this->conn->prepare($sqlQuery);

        $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id)); 
        $stmt->bindParam(":teacher_reg_id", $this->teacher_reg_id); 

        $stmt->execute();
        return $stmt;
    } 

       public function getSingle(){

        $columns = implode(', ', $this->columns);

        $sqlQuery = "SELECT
                     ".$columns." 
                FROM
                    ". $this->db_table ."
                WHERE 
                    reg_id = ?  
                LIMIT 0,1";

        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));

        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->bindParam(1, $this->reg_id); 
        $stmt->execute();
        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $this->reg_id = $dataRow['reg_id'];
        $this->module_title = $dataRow['module_title'];
        
        $this->category_id = $dataRow['category_id'];
        $this->status = $dataRow['status'];
        $this->teacher_reg_id = $dataRow['teacher_reg_id'];

    } 


    public function getByCategoryTeacher(){
        $sqlQuery = "
        SELECT M.reg_id, M.module_title, M.category_id, M.status, M.teacher_reg_id, M.date_added, C.category_name, M.status

        FROM " . $this->db_table . " M
        LEFT JOIN tbl_categories C ON M.category_id = C.id

        WHERE 
        
        M.category_id = :category_id
        AND M.teacher_reg_id = :teacher_reg_id AND M.status = 1";  
    
        $stmt = $this->conn->prepare($sqlQuery); 
         
        $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id)); 
        $this->category_id=htmlspecialchars(strip_tags($this->category_id)); 
        
        $stmt->bindParam(":teacher_reg_id", $this->teacher_reg_id); 
        $stmt->bindParam(":category_id", $this->category_id);
 
        $stmt->execute();
        return $stmt;
        
    }




      public function getByCategory(){

        // $columns = implode(', ', $this->columns);

        // $sqlQuery = "SELECT
        //              ".$columns." 
        //         FROM
        //             ". $this->db_table ."
        //         WHERE 
        //             category_id = ?  
        //         LIMIT 0,1";

        // $this->category_id=htmlspecialchars(strip_tags($this->category_id));

        // $stmt = $this->conn->prepare($sqlQuery);
        // $stmt->bindParam(1, $this->category_id); 
        // $stmt->execute();
        // $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
        
        // $this->reg_id = $dataRow['reg_id'];
        // $this->module_title = $dataRow['module_title'];
        // $this->category_id = $dataRow['category_id'];
        // $this->status = $dataRow['status'];
        // $this->teacher_reg_id = $dataRow['teacher_reg_id'];


        // $stmt->execute();
        $sqlQuery = "
        SELECT M.reg_id, M.module_title, M.category_id, M.status, M.teacher_reg_id, M.date_added, C.category_name
        FROM " . $this->db_table . " M
        LEFT JOIN tbl_categories C ON M.category_id = C.id
        WHERE M.category_id = :category_id ";  

    
        $stmt = $this->conn->prepare($sqlQuery);

        $this->category_id=htmlspecialchars(strip_tags($this->category_id)); 
        $stmt->bindParam(":category_id", $this->category_id); 

        $stmt->execute();
        return $stmt;

    } 

    

    public function getByTitle(){

        // $columns = implode(', ', $this->columns);

        // $sqlQuery = "SELECT
        //     ".$columns."
        // FROM
        //     ". $this->db_table ."
        // WHERE 
        //     module_title = ?  
        // LIMIT 0,1";


        $sqlQuery = "
        SELECT M.reg_id, M.module_title, M.category_id, M.status, M.teacher_reg_id, M.date_added, C.category_name
        FROM " . $this->db_table . " M
        LEFT JOIN tbl_categories C ON M.category_id = C.id
        WHERE M.module_title = ? ";  



        $stmt = $this->conn->prepare($sqlQuery);

        $this->module_title=htmlspecialchars(strip_tags($this->module_title));

        $stmt->bindParam(1, $this->module_title); 
        $stmt->execute();
        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->reg_id = $dataRow['reg_id'];
        $this->module_title = $dataRow['module_title'];
        $this->category_name = $dataRow['category_name'];
        $this->status = $dataRow['status'];
        $this->teacher_reg_id = $dataRow['teacher_reg_id']; 
        $this->category_id = $dataRow['category_id']; 

        

    } 



    public function getByTitleCategoryIDTeacherRegID(){

        

        $sqlQuery = "
        SELECT M.reg_id, M.module_title, M.category_id, M.status, M.teacher_reg_id, M.date_added, C.category_name
        FROM " . $this->db_table . " M
        LEFT JOIN tbl_categories C ON M.category_id = C.id
        WHERE M.module_title = ? AND  M.category_id = ?  AND  M.teacher_reg_id = ?";  



        $stmt = $this->conn->prepare($sqlQuery);

        $this->module_title=htmlspecialchars(strip_tags($this->module_title));
        $this->category_id=htmlspecialchars(strip_tags($this->category_id));
        $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id));

        $stmt->bindParam(1, $this->module_title); 
        $stmt->bindParam(2, $this->category_id); 
        $stmt->bindParam(3, $this->teacher_reg_id); 
        $stmt->execute();
        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->reg_id = $dataRow['reg_id'];
        $this->module_title = $dataRow['module_title'];
        $this->category_name = $dataRow['category_name'];
        $this->status = $dataRow['status'];
        $this->teacher_reg_id = $dataRow['teacher_reg_id']; 
        $this->category_id = $dataRow['category_id']; 
        $this->status = $dataRow['status']; 

        

    } 
    

    public function create(){

        $sqlQuery = "INSERT INTO
                    ". $this->db_table ."
                SET
                    module_title = :module_title, 
                    category_id = :category_id, 
                    status = :status,
                    teacher_reg_id = :teacher_reg_id";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->module_title=htmlspecialchars(strip_tags($this->module_title));
        $this->category_id=htmlspecialchars(strip_tags($this->category_id));
        $this->status=htmlspecialchars(strip_tags($this->status));
        $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id)); 
        
        $stmt->bindParam(":module_title", $this->module_title);
        $stmt->bindParam(":category_id", $this->category_id);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":teacher_reg_id", $this->teacher_reg_id);

        if($stmt->execute()){
            return true;
        }

        

        return false;

    }

    public function update(){

        $sqlQuery = "UPDATE
                    ". $this->db_table ."
                SET
                    module_title = :module_title,
                    category_id = :category_id, 
                    status = :status
                WHERE 
                    reg_id = :reg_id";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->module_title=htmlspecialchars(strip_tags($this->module_title));   
        $this->category_id=htmlspecialchars(strip_tags($this->category_id));
        $this->status=htmlspecialchars(strip_tags($this->status));   
        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));  
 
        $stmt->bindParam(":module_title", $this->module_title);   
        $stmt->bindParam(":category_id", $this->category_id);
        $stmt->bindParam(":status", $this->status);   
        $stmt->bindParam(":reg_id", $this->reg_id);

        if($stmt->execute()){
            return true;
        }

        echo json_encode($stmt->errorInfo(),true);exit;
  
    }

    function delete(){

        $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE reg_id = ?";
        $stmt = $this->conn->prepare($sqlQuery);
    
        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));

        $stmt->bindParam(1, $this->reg_id);

        if($stmt->execute()){
            return true;
        }

        return false;
    }

}