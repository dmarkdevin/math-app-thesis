<?php
class Teachers
{
    // Connection
    private $conn;
    // Table
    private $db_table = "tbl_teachers";
    private $columns = 
        [   
            'reg_id',
            'first_name',
            'last_name',
            'email',
            'password',
            'teacher_id',
            'status',
            'school_reg_id',
            'reg_type',
            'date_added',
            'admin_id'
        ];
    // Columns
    public $reg_id;
    public $first_name;
    public $last_name;
    public $email;
    public $password;
    public $teacher_id;
    public $status;
    public $school_reg_id;
    public $reg_type;
    public $date_added;
    public $admin_id;
    // Db connection
    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function get()
    {
         
        $sqlQuery = "

        SELECT T.reg_id, T.first_name, T.last_name, T.email, T.teacher_id, T.status, SC.school_name,
        T.password , T.admin_id, T.school_reg_id,  T.reg_type, T.date_added
        
        FROM " . $this->db_table . " T
        
        LEFT JOIN tbl_schools SC ON T.school_reg_id = SC.reg_id "; 
        
        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->execute();
        return $stmt;

    }

     

    public function getByEmail(){

        $columns = implode(', ', $this->columns);

        $this->email = htmlspecialchars(strip_tags($this->email)); 

        $sqlQuery = "SELECT ".$columns."
                    FROM
                    ". $this->db_table ."
                WHERE 
                    email = ?  
                LIMIT 0,1";

        $stmt = $this->conn->prepare($sqlQuery);  
        $stmt->bindParam(1, $this->email); 
        $stmt->execute();
        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC); 
        
        $this->reg_id = $dataRow['reg_id']; 
        $this->email = $dataRow['email'];
        $this->password = $dataRow['password'];  
        $this->first_name = $dataRow['first_name'];  
        $this->last_name = $dataRow['last_name'];  
        $this->status = $dataRow['status'];  

    } 

    public function getSingle(){

        $columns = implode(', ', $this->columns);

        $sqlQuery = "SELECT ".$columns."
                FROM
                    ". $this->db_table ."
                WHERE 
                    reg_id = ?  
                LIMIT 0,1";

        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));

        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->bindParam(1, $this->reg_id); 
        $stmt->execute();
        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $this->reg_id = $dataRow['reg_id'];
        $this->email = $dataRow['email'];
        $this->first_name = $dataRow['first_name'];
        $this->last_name = $dataRow['last_name'];
        $this->teacher_id = $dataRow['teacher_id']; 
        $this->status = $dataRow['status']; 
        $this->school_reg_id = $dataRow['school_reg_id']; 
        $this->admin_id = $dataRow['admin_id']; 
        
    } 

    public function create(){

        $sqlQuery = "INSERT INTO
                    ". $this->db_table ."
                SET
                    email = :email, 
                    first_name = :first_name, 
                    last_name = :last_name,  
                    school_reg_id = :school_reg_id,
                
                    password = :password,
                    status = :status,
                    admin_id = :admin_id,
                    reg_type = :reg_type";

                        // teacher_id = :teacher_id,

        $stmt = $this->conn->prepare($sqlQuery);

        $this->email=htmlspecialchars(strip_tags($this->email));
        $this->first_name=htmlspecialchars(strip_tags($this->first_name));
        $this->last_name=htmlspecialchars(strip_tags($this->last_name)); 
        $this->school_reg_id=htmlspecialchars(strip_tags($this->school_reg_id)); 
        // $this->teacher_id=htmlspecialchars(strip_tags($this->teacher_id)); 
        $this->password=htmlspecialchars(strip_tags($this->password)); 
        $this->status=htmlspecialchars(strip_tags($this->status)); 
        $this->admin_id=htmlspecialchars(strip_tags($this->admin_id));
        $this->reg_type=htmlspecialchars(strip_tags($this->reg_type));
    
        $stmt->bindParam(":email", $this->email);
        $stmt->bindParam(":first_name", ($this->first_name) );
        $stmt->bindParam(":last_name", ($this->last_name) ); 
        $stmt->bindParam(":school_reg_id", ($this->school_reg_id) ); 
        // $stmt->bindParam(":teacher_id", ($this->teacher_id) ); 
        $stmt->bindParam(":password", ($this->password) ); 
        $stmt->bindParam(":status", ($this->status) ); 
        $stmt->bindParam(":admin_id", $this->admin_id);
        $stmt->bindParam(":reg_type", $this->reg_type);

        if($stmt->execute()){
            return true;
        }
        // echo json_encode($stmt->errorInfo(),true);exit;

        return false;

    }

    public function update(){


        $newpassword = '';

        if(!empty($this->password)){ 
            $this->password=htmlspecialchars(strip_tags($this->password)); 
            $newpassword = 'password = :password,';  
        }

        $sqlQuery = "UPDATE
                    ". $this->db_table ."
                SET
                    email = :email,
                    first_name = :first_name, 
                    last_name = :last_name,  
                    school_reg_id = :school_reg_id,
                    teacher_id = :teacher_id,
                    ". $newpassword ."
                    status = :status
                        
                WHERE 
                    reg_id = :reg_id";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->email=htmlspecialchars(strip_tags($this->email)); 
        $this->first_name=htmlspecialchars(strip_tags($this->first_name));
        $this->last_name=htmlspecialchars(strip_tags($this->last_name)); 
       
        $this->school_reg_id=htmlspecialchars(strip_tags($this->school_reg_id)); 
        $this->status=htmlspecialchars(strip_tags($this->status)); 
        $this->teacher_id=htmlspecialchars(strip_tags($this->teacher_id)); 

        if(empty($this->school_reg_id)){
            $this->school_reg_id = NULL;
        }
 
 
        $stmt->bindParam(":email", $this->email); 
        $stmt->bindParam(":first_name", strtoupper($this->first_name) );
        $stmt->bindParam(":last_name", strtoupper($this->last_name) ); 
     
        $stmt->bindParam(":school_reg_id", $this->school_reg_id); 
        $stmt->bindParam(":teacher_id", $this->teacher_id); 
        $stmt->bindParam(":status", $this->status); 
        $stmt->bindParam(":reg_id", $this->reg_id);

        if(!empty($newpassword)){
            $stmt->bindParam(":password", $this->password); 
        }

        if($stmt->execute()){
            return true;
        }
        return false;
    }

    function delete(){

        $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE reg_id = ?";
        $stmt = $this->conn->prepare($sqlQuery);
    
        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));

        $stmt->bindParam(1, $this->reg_id);

        if($stmt->execute()){
            return true;
        }

        return false;
    }


    public function verify(){
 
        $sqlQuery = "UPDATE
                    ". $this->db_table ."
                SET 
                    status = 1
                WHERE 
                    reg_id = :reg_id";

        $stmt = $this->conn->prepare($sqlQuery);

        $stmt->bindParam(":reg_id", $this->reg_id);

        if($stmt->execute()){
            return true;
        }
        return false;
    }



}