<?php
class Pupils
{
    // Connection
    private $conn;
    // Table
    private $db_table = "tbl_pupils";
    private $columns = 
    [   
        'reg_id',
        'first_name',
        'last_name',
        'username',
        'password',
        'section_id',
        'status',
        'admin_id',
        'teacher_reg_id',
        'reg_type',
        'date_added',
        'admin_id',
        'student_id',
        'gender',
        'avatar'
    ];
    // Columns
    public $reg_id;
    public $first_name;
    public $last_name;
    public $student_id;
    public $username;
    public $password;   
    public $status;
    public $date_added;
    public $admin_id;
    public $teacher_reg_id;
    public $section_id;
    public $reg_type;

    public $student_idCheck;

    public $section_name,$q1,$q2,$q3,$q4,$section_code,$n1,$n2;
    
    public $gender,$avatar;
    // Db connection
    public function __construct($db)
    {
        $this->conn = $db;
    }
    public function get(){

        $newsection_id = '';

        if(!empty($this->section_id)){ 
            $this->section_id=htmlspecialchars(strip_tags($this->section_id)); 
            $newsection_id = 'P.section_id = :section_id AND ';  
        }

        $sqlQuery = "
            SELECT P.reg_id, P.username, P.section_id, P.status, S.section_name,
            P.first_name, P.last_name, P.student_id
            
            FROM " . $this->db_table . " P
            LEFT JOIN tbl_sections  S ON P.section_id = S.reg_id
            
            WHERE ".$newsection_id." P.teacher_reg_id = :teacher_reg_id ORDER BY P.last_name ASC"; 

        $stmt = $this->conn->prepare($sqlQuery); 

        $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id)); 
        $stmt->bindParam(":teacher_reg_id", $this->teacher_reg_id); 

        if(!empty($newsection_id)){
            $stmt->bindParam(":section_id", $this->section_id); 
        }
        
        $stmt->execute();
        return $stmt;
    } 

    public function getAll(){

        $sqlQuery = "
            SELECT P.reg_id, P.username, P.section_id, P.status, S.section_name
            
            FROM " . $this->db_table . " P
            JOIN tbl_sections  S ON P.section_id = S.reg_id"; 

        $stmt = $this->conn->prepare($sqlQuery); 

        $stmt->execute();
        return $stmt;
    } 

    public function getClass(){ 

        $sqlQuery = "
                SELECT P.reg_id, P.username, P.section_id, P.status, P.first_name, P.last_name, P.student_id
                , S.section_name, S.q1,S.q2,S.q3,S.q4,S.teacher_reg_id ,S.n1,S.n2, P.avatar

                FROM " . $this->db_table . " P
                LEFT JOIN tbl_sections  S ON P.section_id = S.reg_id
                LEFT JOIN tbl_teachers  T ON P.teacher_reg_id = T.reg_id
                
                WHERE P.reg_id = ? "; 


        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));

        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->bindParam(1, $this->reg_id); 
        $stmt->execute();

        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $this->reg_id = $dataRow['reg_id'];
        $this->first_name = $dataRow['first_name'];
        $this->last_name = $dataRow['last_name'];

        $this->student_id = $dataRow['student_id'];

        $this->username = $dataRow['username'];
        
        $this->status = $dataRow['status'];

        $this->section_id = $dataRow['section_id'];
        $this->section_name = $dataRow['section_name'];
        $this->q1 = $dataRow['q1'];
        $this->q2 = $dataRow['q2'];
        $this->q3 = $dataRow['q3'];
        $this->q4 = $dataRow['q4'];
        $this->n1 = $dataRow['n1'];
        $this->n2 = $dataRow['n2'];
        $this->teacher_reg_id = $dataRow['teacher_reg_id'];
        
        $this->avatar = $dataRow['avatar'];
        
        

    }  

    public function getBySectionCode(){

        $sqlQuery = "
            SELECT P.reg_id, P.username, P.section_id, P.status, S.section_name, S.section_code,
            P.first_name, P.last_name, P.student_id, P.avatar
            
            FROM " . $this->db_table . " P
            LEFT JOIN tbl_sections  S ON P.section_id = S.reg_id
            
            WHERE S.section_code = :section_code AND P.status = 1"; 

        $stmt = $this->conn->prepare($sqlQuery); 

        $this->section_code=htmlspecialchars(strip_tags($this->section_code)); 
        $stmt->bindParam(":section_code", $this->section_code); 

        $stmt->execute();
        return $stmt;
    } 


    public function getSingle(){

        $columns = implode(', ', $this->columns);

        $sqlQuery = "SELECT ".$columns."
                FROM
                    ". $this->db_table ."
                WHERE 
                    reg_id = ?  
                LIMIT 0,1";

        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));

        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->bindParam(1, $this->reg_id); 
        $stmt->execute();
        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $this->reg_id = $dataRow['reg_id'];
        $this->first_name = $dataRow['first_name'];
        $this->last_name = $dataRow['last_name'];

        $this->student_id = $dataRow['student_id'];

        $this->username = $dataRow['username'];
        $this->password = $dataRow['password'];
        $this->section_id = $dataRow['section_id'];
        $this->status = $dataRow['status'];
        $this->teacher_reg_id = $dataRow['teacher_reg_id'];
        $this->gender = $dataRow['gender'];
        $this->avatar = $dataRow['avatar'];

    } 

    public function getByUsername(){

        $columns = implode(', ', $this->columns);


        $sqlQuery = "SELECT ".$columns."
                        FROM
                            ". $this->db_table ."
                        WHERE 
                            username = ?  
                        LIMIT 0,1";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->username=htmlspecialchars(strip_tags($this->username));

        $stmt->bindParam(1, $this->username); 
        $stmt->execute();
        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->reg_id = $dataRow['reg_id'];
        $this->username = $dataRow['username'];
        $this->password = $dataRow['password'];
        $this->section_id = $dataRow['section_id'];
        $this->status = $dataRow['status'];
      
        $this->teacher_reg_id = $dataRow['teacher_reg_id']; 

    } 


    public function getByStudentIDSection(){

        $columns = implode(', ', $this->columns);


        $sqlQuery = "SELECT ".$columns."
                        FROM
                            ". $this->db_table ."
                        WHERE 
                        
                            student_id = ?  AND
                            section_id = ?  
                                
                        LIMIT 0,1";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->student_id=htmlspecialchars(strip_tags($this->student_id));
        $this->section_id=htmlspecialchars(strip_tags($this->section_id));

        $stmt->bindParam(1, $this->student_id); 
        $stmt->bindParam(2, $this->section_id); 
        $stmt->execute();
        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->reg_id = $dataRow['reg_id'];
        $this->username = $dataRow['username'];
        $this->password = $dataRow['password'];
        $this->section_id = $dataRow['section_id'];
        $this->status = $dataRow['status'];
        $this->student_id = $dataRow['student_id'];
        $this->student_idCheck = $dataRow['student_id'];
      
        $this->teacher_reg_id = $dataRow['teacher_reg_id']; 
        
    } 



    public function create(){

        $sqlQuery = "INSERT INTO
                    ". $this->db_table ."
                SET
                    first_name = :first_name,
                    last_name = :last_name,
                    student_id = :student_id,
                    username = :username,
                    password = :password,
                    status = :status,
                    section_id = :section_id, 
                    reg_type = :reg_type,
                    teacher_reg_id = :teacher_reg_id,
                    gender = :gender,
                    avatar = :avatar "; 

        $stmt = $this->conn->prepare($sqlQuery);

        $this->first_name=htmlspecialchars(strip_tags($this->first_name));
        $this->last_name=htmlspecialchars(strip_tags($this->last_name));
        $this->student_id=htmlspecialchars(strip_tags($this->student_id));
        $this->username=htmlspecialchars(strip_tags($this->username));
        $this->password=htmlspecialchars(strip_tags($this->password)); 
        $this->status=htmlspecialchars(strip_tags($this->status)); 
        $this->section_id=htmlspecialchars(strip_tags($this->section_id)); 
        $this->reg_type=htmlspecialchars(strip_tags($this->reg_type)); 
        $this->teacher_reg_id=htmlspecialchars(strip_tags($this->teacher_reg_id)); 
        $this->gender=htmlspecialchars(strip_tags($this->gender)); 
        $this->avatar=htmlspecialchars(strip_tags($this->avatar)); 
      
        $stmt->bindParam(":first_name", $this->first_name);
        $stmt->bindParam(":last_name", $this->last_name);
        $stmt->bindParam(":student_id", $this->student_id);
        $stmt->bindParam(":username", $this->username);
        $stmt->bindParam(":password", ($this->password) ); 
        $stmt->bindParam(":status", ($this->status) ); 
        $stmt->bindParam(":section_id", $this->section_id); 
        $stmt->bindParam(":reg_type", $this->reg_type); 
        $stmt->bindParam(":teacher_reg_id", $this->teacher_reg_id); 
        $stmt->bindParam(":gender", $this->gender); 
        $stmt->bindParam(":avatar", $this->avatar); 


        if($stmt->execute()){
            return true;
        }
 

        return false;

    }

    public function update(){

        $newpassword = '';

        if(!empty($this->password)){   
            $newpassword = 'password = :password,';  
        }


        $sqlQuery = "UPDATE
                    ". $this->db_table ."
                SET
                    first_name = :first_name,
                    last_name = :last_name,
                    student_id = :student_id,

                    username = :username,
                    ". $newpassword ."
                    section_id = :section_id,
                    status = :status,
                    gender = :gender
                WHERE 
                    reg_id = :reg_id";

        $stmt = $this->conn->prepare($sqlQuery);

        $this->first_name=htmlspecialchars(strip_tags($this->first_name));
        $this->last_name=htmlspecialchars(strip_tags($this->last_name));
        $this->student_id=htmlspecialchars(strip_tags($this->student_id));

        $this->username=htmlspecialchars(strip_tags($this->username));   
        $this->status=htmlspecialchars(strip_tags($this->status));   
        $this->section_id=htmlspecialchars(strip_tags($this->section_id));   
        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));  
        $this->gender=htmlspecialchars(strip_tags($this->gender));  
 
        $stmt->bindParam(":first_name", $this->first_name);
        $stmt->bindParam(":last_name", $this->last_name);
        $stmt->bindParam(":student_id", $this->student_id);

        $stmt->bindParam(":username", $this->username);   
        $stmt->bindParam(":status", $this->status);   
        $stmt->bindParam(":section_id", $this->section_id);   
        $stmt->bindParam(":gender", $this->gender);   
        $stmt->bindParam(":reg_id", $this->reg_id);

        if(!empty($newpassword)){
            $this->password=htmlspecialchars(strip_tags($this->password));  
            $stmt->bindParam(":password", $this->password);   
        }
        


        if($stmt->execute()){
            return true;
        }
        return false;
    }

    function delete(){

        $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE reg_id = ?";
        $stmt = $this->conn->prepare($sqlQuery);
    
        $this->reg_id=htmlspecialchars(strip_tags($this->reg_id));

        $stmt->bindParam(1, $this->reg_id);

        if($stmt->execute()){
            return true;
        }

        return false;
    }

}