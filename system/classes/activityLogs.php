<?php
class ActivityLogs
{
    private $conn;
    private $db_table = "tbl_acitivity_logs";
    private $columns = ['id','account_type','account_id','activity','description','ip_address','date_added'];
     
    public $id;
    public $account_type;
    public $account_id;
    public $activity;
    public $description;
    public $ip_address;
    public $date_added;
    public function __construct($db)
    {
        $this->conn = $db;
    }
    

    public function get()
    { 

        
        // LEFT JOIN tbl_pupils P ON P.reg_id = R.pupil_id
        // LEFT JOIN tbl_pupils P ON P.reg_id = R.pupil_id
        // LEFT JOIN tbl_pupils P ON P.reg_id = R.pupil_id
        $sqlQuery = "
            SELECT * 

            FROM " . $this->db_table . " AL
           
            WHERE AL.account_id = :account_id AND 
            AL.account_type = :account_type ORDER by date_added DESC"; 

        $stmt = $this->conn->prepare($sqlQuery); 

        $this->account_id=htmlspecialchars(strip_tags($this->account_id)); 
        $stmt->bindParam(":account_id", $this->account_id); 
       
        $this->account_type=htmlspecialchars(strip_tags($this->account_type)); 
        $stmt->bindParam(":account_type", $this->account_type); 

        $stmt->execute();
        return $stmt;

    }




    
    public function create(){

        $sqlQuery = "INSERT INTO
                    ". $this->db_table ."
                SET
                    account_type = :account_type,
                    account_id = :account_id,
                    activity = :activity,
                    description = :description,
                    ip_address = :ip_address 
                    "; 

        $stmt = $this->conn->prepare($sqlQuery);

        $this->account_type=htmlspecialchars(strip_tags($this->account_type));
        $this->account_id=htmlspecialchars(strip_tags($this->account_id));
        $this->activity=htmlspecialchars(strip_tags($this->activity));
        $this->description=htmlspecialchars(strip_tags($this->description));
        $this->ip_address=htmlspecialchars(strip_tags($this->ip_address));
      
        $stmt->bindParam(":account_type", $this->account_type);
        $stmt->bindParam(":account_id", $this->account_id);
        $stmt->bindParam(":activity", $this->activity);
        $stmt->bindParam(":description", $this->description);
        $stmt->bindParam(":ip_address", $this->ip_address); 

        if($stmt->execute()){
            return true;
        }
 
        return false;

    }


}