-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for mathappv4
CREATE DATABASE IF NOT EXISTS `mathappv4` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `mathappv4`;

-- Dumping structure for table mathappv4.tbl_acitivity_logs
CREATE TABLE IF NOT EXISTS `tbl_acitivity_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_type` varchar(11) DEFAULT '0',
  `account_id` int(11) DEFAULT '0',
  `activity` varchar(255) DEFAULT '0',
  `description` varchar(255) DEFAULT '0',
  `ip_address` varchar(255) DEFAULT '0',
  `date_added` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Dumping data for table mathappv4.tbl_acitivity_logs: ~12 rows (approximately)
/*!40000 ALTER TABLE `tbl_acitivity_logs` DISABLE KEYS */;
INSERT IGNORE INTO `tbl_acitivity_logs` (`id`, `account_type`, `account_id`, `activity`, `description`, `ip_address`, `date_added`) VALUES
	(1, 'admin', 1, 'admin login', '/a/login-submit.php', '', '2023-01-24 21:18:58'),
	(2, 'admin', 1, 'admin login', '/a/login-submit.php', '', '2023-01-24 21:19:31'),
	(3, 'teacher', 1, 'teacher login', '/t/login-submit.php', '', '2023-01-24 21:19:59'),
	(4, 'teacher', 1, 'teacher login', '/t/login-submit.php', '', '2023-01-30 06:11:56'),
	(5, 'teacher', 1, 'teacher login', '/t/login-submit.php', '', '2023-01-30 17:27:17'),
	(6, 'teacher', 1, 'teacher login', '/t/login-submit.php', '', '2023-01-30 20:24:23'),
	(7, 'admin', 1, 'admin login', '/a/login-submit.php', '', '2023-02-02 01:59:43'),
	(8, 'teacher', 1, 'teacher login', '/t/login-submit.php', '', '2023-02-02 16:23:35'),
	(9, 'teacher', 1, 'teacher login', '/t/login-submit.php', '', '2023-02-03 00:19:55'),
	(10, 'teacher', 1, 'teacher add module', '/t/add_module-submit.php', '', '2023-02-03 00:20:17'),
	(11, 'teacher', 1, 'teacher login', '/t/login-submit.php', '', '2023-02-03 03:04:03'),
	(12, 'teacher', 1, 'teacher login', '/t/login-submit.php', '', '2023-02-03 03:43:01'),
	(13, 'teacher', 1, 'teacher login', '/t/login-submit.php', '', '2023-02-03 04:46:17');
/*!40000 ALTER TABLE `tbl_acitivity_logs` ENABLE KEYS */;

-- Dumping structure for table mathappv4.tbl_admin
CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table mathappv4.tbl_admin: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_admin` DISABLE KEYS */;
INSERT IGNORE INTO `tbl_admin` (`id`, `name`, `email`, `password`, `status`) VALUES
	(1, 'administrator', 'admin@mathapp3.com', '$2y$10$EvX4CnddLmaCmYlgS8xqzeJViY2judtdNAgbIOBgEG1vgF1Ban/KG', 1);
/*!40000 ALTER TABLE `tbl_admin` ENABLE KEYS */;

-- Dumping structure for table mathappv4.tbl_categories
CREATE TABLE IF NOT EXISTS `tbl_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_name` (`category_name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table mathappv4.tbl_categories: ~4 rows (approximately)
/*!40000 ALTER TABLE `tbl_categories` DISABLE KEYS */;
INSERT IGNORE INTO `tbl_categories` (`id`, `category_name`) VALUES
	(1, 'Quarter 1'),
	(2, 'Quarter 2'),
	(3, 'Quarter 3'),
	(4, 'Quarter 4');
/*!40000 ALTER TABLE `tbl_categories` ENABLE KEYS */;

-- Dumping structure for table mathappv4.tbl_lessons
CREATE TABLE IF NOT EXISTS `tbl_lessons` (
  `reg_id` int(11) NOT NULL AUTO_INCREMENT,
  `lesson_title` varchar(50) NOT NULL,
  `lesson_file` varchar(50) DEFAULT NULL,
  `lesson_description` text,
  `module_id` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `teacher_reg_id` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`reg_id`),
  KEY `module_id` (`module_id`),
  KEY `teacher_reg_id` (`teacher_reg_id`),
  CONSTRAINT `tbl_lessons_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `tbl_modules` (`reg_id`) ON DELETE SET NULL,
  CONSTRAINT `tbl_lessons_ibfk_2` FOREIGN KEY (`teacher_reg_id`) REFERENCES `tbl_teachers` (`reg_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table mathappv4.tbl_lessons: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_lessons` DISABLE KEYS */;
INSERT IGNORE INTO `tbl_lessons` (`reg_id`, `lesson_title`, `lesson_file`, `lesson_description`, `module_id`, `status`, `teacher_reg_id`, `date_added`) VALUES
	(1, 'lesson1', NULL, NULL, 1, 1, 1, '2023-02-03 00:20:27');
/*!40000 ALTER TABLE `tbl_lessons` ENABLE KEYS */;

-- Dumping structure for table mathappv4.tbl_modules
CREATE TABLE IF NOT EXISTS `tbl_modules` (
  `reg_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_title` varchar(50) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `teacher_reg_id` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`reg_id`),
  KEY `category_id` (`category_id`),
  KEY `teacher_reg_id` (`teacher_reg_id`),
  CONSTRAINT `tbl_modules_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `tbl_categories` (`id`) ON DELETE SET NULL,
  CONSTRAINT `tbl_modules_ibfk_2` FOREIGN KEY (`teacher_reg_id`) REFERENCES `tbl_teachers` (`reg_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table mathappv4.tbl_modules: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_modules` DISABLE KEYS */;
INSERT IGNORE INTO `tbl_modules` (`reg_id`, `module_title`, `category_id`, `status`, `teacher_reg_id`, `date_added`) VALUES
	(1, 'MODULE1', 1, 1, 1, '2023-02-03 00:20:17');
/*!40000 ALTER TABLE `tbl_modules` ENABLE KEYS */;

-- Dumping structure for table mathappv4.tbl_numeracy_questions
CREATE TABLE IF NOT EXISTS `tbl_numeracy_questions` (
  `reg_id` int(11) NOT NULL AUTO_INCREMENT,
  `number1` text,
  `number2` text,
  `answer` varchar(50) DEFAULT NULL,
  `operator` int(11) DEFAULT NULL,
  `numeracy` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `teacher_reg_id` int(11) NOT NULL,
  `date_added` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`reg_id`),
  KEY `teacher_reg_id` (`teacher_reg_id`),
  CONSTRAINT `tbl_numeracy_questions_ibfk_1` FOREIGN KEY (`teacher_reg_id`) REFERENCES `tbl_teachers` (`reg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table mathappv4.tbl_numeracy_questions: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_numeracy_questions` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_numeracy_questions` ENABLE KEYS */;

-- Dumping structure for table mathappv4.tbl_numeracy_results
CREATE TABLE IF NOT EXISTS `tbl_numeracy_results` (
  `reg_id` int(11) NOT NULL AUTO_INCREMENT,
  `addition` int(11) DEFAULT '0',
  `subtraction` int(11) DEFAULT '0',
  `multiplication` int(11) DEFAULT '0',
  `division` int(11) DEFAULT '0',
  `right_answer` int(11) DEFAULT '0',
  `wrong_answer` int(11) DEFAULT '0',
  `unanswered` int(11) DEFAULT '0',
  `pupil_id` int(11) DEFAULT NULL,
  `numeracy` int(1) DEFAULT NULL,
  `date_added` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`reg_id`),
  KEY `pupil_id` (`pupil_id`),
  CONSTRAINT `tbl_numeracy_results_ibfk_1` FOREIGN KEY (`pupil_id`) REFERENCES `tbl_pupils` (`reg_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table mathappv4.tbl_numeracy_results: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_numeracy_results` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_numeracy_results` ENABLE KEYS */;

-- Dumping structure for table mathappv4.tbl_pupils
CREATE TABLE IF NOT EXISTS `tbl_pupils` (
  `reg_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `avatar` int(11) DEFAULT NULL,
  `student_id` varchar(50) DEFAULT NULL,
  `section_id` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `teacher_reg_id` int(11) DEFAULT NULL,
  `reg_type` varchar(50) NOT NULL,
  `date_added` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`reg_id`),
  KEY `section_id` (`section_id`),
  KEY `admin_id` (`admin_id`),
  KEY `teacher_reg_id` (`teacher_reg_id`),
  CONSTRAINT `tbl_pupils_ibfk_1` FOREIGN KEY (`section_id`) REFERENCES `tbl_sections` (`reg_id`),
  CONSTRAINT `tbl_pupils_ibfk_2` FOREIGN KEY (`admin_id`) REFERENCES `tbl_admin` (`id`) ON DELETE SET NULL,
  CONSTRAINT `tbl_pupils_ibfk_3` FOREIGN KEY (`teacher_reg_id`) REFERENCES `tbl_teachers` (`reg_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- Dumping data for table mathappv4.tbl_pupils: ~23 rows (approximately)
/*!40000 ALTER TABLE `tbl_pupils` DISABLE KEYS */;
INSERT IGNORE INTO `tbl_pupils` (`reg_id`, `username`, `password`, `first_name`, `last_name`, `gender`, `avatar`, `student_id`, `section_id`, `status`, `admin_id`, `teacher_reg_id`, `reg_type`, `date_added`) VALUES
	(1, '120775190026', '55', 'Fredmark ', 'Abante', 'F', NULL, '120775190026', 1, 1, NULL, 1, 'teacher', '2023-01-24 21:20:45'),
	(2, '120775190020', '15', 'Mc Jomer ', 'Aguilon', NULL, NULL, '120775190020', 1, 1, NULL, 1, 'teacher', '2023-01-24 21:20:45'),
	(3, '120775190051', '99', 'Kenth Vhease', 'Arpon', NULL, NULL, '120775190051', 1, 1, NULL, 1, 'teacher', '2023-01-24 21:20:45'),
	(4, '120775190006', '52', 'Carl Eshieben', 'Cahapay', NULL, NULL, '120775190006', 1, 1, NULL, 1, 'teacher', '2023-01-24 21:20:45'),
	(5, '120775190015', '74', 'Mark Jonel ', 'Cerilles', NULL, NULL, '120775190015', 1, 1, NULL, 1, 'teacher', '2023-01-24 21:20:45'),
	(6, '120775190030', '78', 'Haiyan John ', 'Garillo', NULL, NULL, '120775190030', 1, 1, NULL, 1, 'teacher', '2023-01-24 21:20:45'),
	(7, '120775190040', '83', 'Randy ', 'Garillo', NULL, NULL, '120775190040', 1, 1, NULL, 1, 'teacher', '2023-01-24 21:20:45'),
	(8, '120775190031', '16', 'Migurel Louis', 'Mullon', NULL, NULL, '120775190031', 1, 1, NULL, 1, 'teacher', '2023-01-24 21:20:45'),
	(9, '120775190005', '33', 'Donnie', 'Corpin', NULL, NULL, '120775190005', 1, 1, NULL, 1, 'teacher', '2023-01-24 21:20:45'),
	(10, '120775190022', '67', 'Isariel', 'Napalit', NULL, NULL, '120775190022', 1, 1, NULL, 1, 'teacher', '2023-01-24 21:20:45'),
	(11, '120775190016', '12', 'Yun Fred ', 'Naval', NULL, NULL, '120775190016', 1, 1, NULL, 1, 'teacher', '2023-01-24 21:20:45'),
	(12, '120775190049', '92', 'Edwin ', 'Palconit', NULL, NULL, '120775190049', 1, 1, NULL, 1, 'teacher', '2023-01-24 21:20:45'),
	(13, '120775190008', '71', 'Jade', 'Pinon', NULL, NULL, '120775190008', 1, 1, NULL, 1, 'teacher', '2023-01-24 21:20:45'),
	(14, '120775190012', '36', 'Jacob ', 'Saboy ', NULL, NULL, '120775190012', 1, 1, NULL, 1, 'teacher', '2023-01-24 21:20:45'),
	(15, '120775190050', '55', 'Justin Louis ', 'Saboy ', NULL, NULL, '120775190050', 1, 1, NULL, 1, 'teacher', '2023-01-24 21:20:45'),
	(16, '120775190044', '84', 'Aliah Kaith', 'Bacalla', NULL, NULL, '120775190044', 1, 1, NULL, 1, 'teacher', '2023-01-24 21:20:45'),
	(17, '120775180007', '13', 'Lowielyn ', 'Bareja', NULL, NULL, '120775180007', 1, 1, NULL, 1, 'teacher', '2023-01-24 21:20:45'),
	(18, '120775190107', '93', 'Yuri Jean ', 'Carmen', NULL, NULL, '120775190107', 1, 1, NULL, 1, 'teacher', '2023-01-24 21:20:45'),
	(19, '120775190043', '48', 'Katherine ', 'De Guzman', NULL, NULL, '120775190043', 1, 1, NULL, 1, 'teacher', '2023-01-24 21:20:45'),
	(20, '120775190045', '42', 'Christine Joy', 'Ecat', NULL, NULL, '120775190045', 1, 1, NULL, 1, 'teacher', '2023-01-24 21:20:45'),
	(21, '120775190004', '39', 'Solin ', 'Garillo', NULL, NULL, '120775190004', 1, 1, NULL, 1, 'teacher', '2023-01-24 21:20:45'),
	(22, '120775190162', '63', 'Jalien', 'Pingul', NULL, NULL, '120775190162', 1, 1, NULL, 1, 'teacher', '2023-01-24 21:20:45'),
	(23, '120775190011', '98', 'Pebbie Lyn', 'Rosalies', NULL, NULL, '120775190011', 1, 1, NULL, 1, 'teacher', '2023-01-24 21:20:45'),
	(24, '1234', '1234', 'test', 'add', 'F', NULL, '1234', 1, 1, NULL, 1, 'teacher', '2023-02-03 04:54:59');
/*!40000 ALTER TABLE `tbl_pupils` ENABLE KEYS */;

-- Dumping structure for table mathappv4.tbl_questions
CREATE TABLE IF NOT EXISTS `tbl_questions` (
  `reg_id` int(11) NOT NULL AUTO_INCREMENT,
  `question` text,
  `option1` varchar(50) DEFAULT NULL,
  `option2` varchar(50) DEFAULT NULL,
  `option3` varchar(50) DEFAULT NULL,
  `option4` varchar(50) DEFAULT NULL,
  `answer` varchar(50) DEFAULT NULL,
  `imagefile` varchar(255) DEFAULT NULL,
  `lesson_id` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `teacher_reg_id` int(11) NOT NULL,
  `date_added` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`reg_id`),
  KEY `lesson_id` (`lesson_id`),
  KEY `teacher_reg_id` (`teacher_reg_id`),
  CONSTRAINT `tbl_questions_ibfk_1` FOREIGN KEY (`lesson_id`) REFERENCES `tbl_lessons` (`reg_id`) ON DELETE SET NULL,
  CONSTRAINT `tbl_questions_ibfk_2` FOREIGN KEY (`teacher_reg_id`) REFERENCES `tbl_teachers` (`reg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table mathappv4.tbl_questions: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_questions` DISABLE KEYS */;
INSERT IGNORE INTO `tbl_questions` (`reg_id`, `question`, `option1`, `option2`, `option3`, `option4`, `answer`, `imagefile`, `lesson_id`, `status`, `teacher_reg_id`, `date_added`) VALUES
	(1, 'hello world', '1', '2', '3', '4', '1', '', 1, 1, 1, '2023-02-03 00:21:07');
/*!40000 ALTER TABLE `tbl_questions` ENABLE KEYS */;

-- Dumping structure for table mathappv4.tbl_results_quiz
CREATE TABLE IF NOT EXISTS `tbl_results_quiz` (
  `reg_id` int(11) NOT NULL AUTO_INCREMENT,
  `right_answer` int(11) DEFAULT '0',
  `wrong_answer` int(11) DEFAULT '0',
  `unanswered` int(11) DEFAULT '0',
  `pupil_id` int(11) DEFAULT NULL,
  `lesson_id` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `date_added` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`reg_id`),
  KEY `lesson_id` (`lesson_id`),
  KEY `pupil_id` (`pupil_id`),
  CONSTRAINT `tbl_results_quiz_ibfk_1` FOREIGN KEY (`lesson_id`) REFERENCES `tbl_lessons` (`reg_id`) ON DELETE SET NULL,
  CONSTRAINT `tbl_results_quiz_ibfk_2` FOREIGN KEY (`pupil_id`) REFERENCES `tbl_pupils` (`reg_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Dumping data for table mathappv4.tbl_results_quiz: ~11 rows (approximately)
/*!40000 ALTER TABLE `tbl_results_quiz` DISABLE KEYS */;
INSERT IGNORE INTO `tbl_results_quiz` (`reg_id`, `right_answer`, `wrong_answer`, `unanswered`, `pupil_id`, `lesson_id`, `status`, `date_added`) VALUES
	(1, 0, 1, 0, 1, 1, NULL, '2023-02-03 00:22:17'),
	(2, 1, 0, 0, 1, 1, NULL, '2023-02-03 00:23:41'),
	(3, 0, 1, 0, 1, 1, NULL, '2023-02-03 00:29:31'),
	(4, 0, 1, 0, 1, 1, NULL, '2023-02-03 01:22:17'),
	(5, 0, 0, 1, 1, 1, NULL, '2023-02-03 02:20:05'),
	(6, 1, 0, 0, 1, 1, NULL, '2023-02-03 02:24:13'),
	(7, 0, 0, 1, 1, 1, NULL, '2023-02-03 02:58:55'),
	(8, 1, 0, 0, 1, 1, NULL, '2023-02-03 02:59:21'),
	(9, 1, 0, 0, 1, 1, NULL, '2023-02-03 03:03:03'),
	(10, 0, 1, 0, 1, 1, NULL, '2023-02-03 03:19:39'),
	(11, 0, 1, 0, 1, 1, NULL, '2023-02-03 03:42:44'),
	(12, 1, 0, 0, 1, 1, NULL, '2023-02-03 04:45:40');
/*!40000 ALTER TABLE `tbl_results_quiz` ENABLE KEYS */;

-- Dumping structure for table mathappv4.tbl_schools
CREATE TABLE IF NOT EXISTS `tbl_schools` (
  `reg_id` int(11) NOT NULL AUTO_INCREMENT,
  `school_name` varchar(50) NOT NULL,
  `school_address` text,
  `school_id` int(11) NOT NULL,
  `language` varchar(11) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`reg_id`),
  UNIQUE KEY `school_name` (`school_name`),
  KEY `admin_id` (`admin_id`),
  CONSTRAINT `tbl_schools_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `tbl_admin` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- Dumping data for table mathappv4.tbl_schools: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_schools` DISABLE KEYS */;
INSERT IGNORE INTO `tbl_schools` (`reg_id`, `school_name`, `school_address`, `school_id`, `language`, `status`, `admin_id`, `date_added`) VALUES
	(19, 'Julita Elementary School', 'baranga', 123456, 'waray', 1, 1, '2023-01-16 02:41:13'),
	(20, 'Burabod Elementary School ', 'barangay Burabod', 1111, NULL, 1, 1, '2023-02-02 02:00:05');
/*!40000 ALTER TABLE `tbl_schools` ENABLE KEYS */;

-- Dumping structure for table mathappv4.tbl_sections
CREATE TABLE IF NOT EXISTS `tbl_sections` (
  `reg_id` int(11) NOT NULL AUTO_INCREMENT,
  `section_name` varchar(50) NOT NULL,
  `section_code` varchar(50) NOT NULL,
  `status` int(1) NOT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `q1` int(1) DEFAULT '1',
  `q2` int(1) DEFAULT '1',
  `q3` int(1) DEFAULT '1',
  `q4` int(1) DEFAULT '1',
  `n1` int(1) DEFAULT NULL,
  `n2` int(1) DEFAULT NULL,
  `teacher_reg_id` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`reg_id`),
  UNIQUE KEY `section_code` (`section_code`),
  KEY `admin_id` (`admin_id`),
  KEY `teacher_reg_id` (`teacher_reg_id`),
  CONSTRAINT `tbl_sections_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `tbl_admin` (`id`) ON DELETE SET NULL,
  CONSTRAINT `tbl_sections_ibfk_2` FOREIGN KEY (`teacher_reg_id`) REFERENCES `tbl_teachers` (`reg_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table mathappv4.tbl_sections: ~3 rows (approximately)
/*!40000 ALTER TABLE `tbl_sections` DISABLE KEYS */;
INSERT IGNORE INTO `tbl_sections` (`reg_id`, `section_name`, `section_code`, `status`, `admin_id`, `q1`, `q2`, `q3`, `q4`, `n1`, `n2`, `teacher_reg_id`, `date_added`) VALUES
	(1, 'section 1', '000006', 1, NULL, 1, 0, 0, 0, 0, 0, 1, '2023-01-24 21:20:31'),
	(7, 'section 2', '000007', 0, NULL, 0, 0, 0, 0, 0, 0, 1, '2023-01-30 20:24:34'),
	(8, 'section 3', '000008', 0, NULL, 0, 0, 0, 0, 0, 0, 1, '2023-01-30 20:24:38');
/*!40000 ALTER TABLE `tbl_sections` ENABLE KEYS */;

-- Dumping structure for table mathappv4.tbl_teachers
CREATE TABLE IF NOT EXISTS `tbl_teachers` (
  `reg_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `school_reg_id` int(11) DEFAULT NULL,
  `reg_type` varchar(50) NOT NULL,
  `date_added` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`reg_id`),
  UNIQUE KEY `email` (`email`),
  KEY `admin_id` (`admin_id`),
  KEY `school_reg_id` (`school_reg_id`),
  CONSTRAINT `tbl_teachers_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `tbl_admin` (`id`) ON DELETE SET NULL,
  CONSTRAINT `tbl_teachers_ibfk_2` FOREIGN KEY (`school_reg_id`) REFERENCES `tbl_schools` (`reg_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table mathappv4.tbl_teachers: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_teachers` DISABLE KEYS */;
INSERT IGNORE INTO `tbl_teachers` (`reg_id`, `email`, `password`, `first_name`, `last_name`, `teacher_id`, `status`, `admin_id`, `school_reg_id`, `reg_type`, `date_added`) VALUES
	(1, 'dmarkdevin@gmail.com', '$2y$10$KHRq7fhpuC8UOiESCmIDBuzayCRQAVB41wBGiw6KlzIx2HIYgni1S', 'demo', 'demo', NULL, 1, 1, 19, '', '2023-01-24 21:19:47');
/*!40000 ALTER TABLE `tbl_teachers` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
