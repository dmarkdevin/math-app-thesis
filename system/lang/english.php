<?php

$lang['english']['WELCOME_TO'] = 'Welcome to';


$lang['waray']['WELCOME_SA'] = 'Welcome sa';


$lang['bisaya']['WELCOME_SA'] = 'Welcome sa';

$lang['english']['GOOD_DAY'] = 'Good day!';


$lang['waray']['GOOD_DAY'] = 'Maayung Adlaw!';


$lang['bisaya']['GOOD_DAY'] = 'Maayong Adlaw!';



$lang['english']['I_AM _YOUR_TEACHER'] = 'I am your Teacher';
$lang['waray']['I_AM _YOUR_TEACHER'] = 'Ang imong Magtutudlo';
$lang['bisaya']['I_AM _YOUR_TEACHER'] = 'Ang imong Magtutudlo';


$lang['english']['LOGIN_TO_YOUR_ACCOUNT'] = 'Login to your account name';
$lang['waray']['LOGIN_TO_YOUR_ACCOUNT'] = 'Pagsulod ha imong ngalan sa account';
$lang['bisaya']['LOGIN_TO_YOUR_ACCOUNT'] = 'Pagsulod ha imong ngalan sa account';


$lang['english']['IS_IT_YOU'] = 'Is it you';
$lang['waray']['IS_IT_YOU'] = 'Ikaw ba si';
$lang['bisaya']['IS_IT_YOU'] = 'Ikaw ba hi';

$lang['english']['YES'] = 'Yes';
$lang['waray']['YES'] = 'Oo';
$lang['bisaya']['YES'] = 'Oo';
 
$lang['english']['NO'] = 'No';
$lang['waray']['NO'] = 'Diri';
$lang['bisaya']['NO'] = 'Dili';

$lang['english']['TAKE_QUIZ'] = 'Take Quiz';
$lang['waray']['TAKE_QUIZ'] = 'Kuha han Quiz';
$lang['bisaya']['TAKE_QUIZ'] = 'Kuha ug Quiz';

$lang['english']['QUIZ_GAME'] = 'Quiz Game';
$lang['waray']['QUIZ_GAME'] = 'Uyag han Quiz';
$lang['bisaya']['QUIZ_GAME'] = 'Dula nga Quiz';
 
$lang['english']['SIGN_OUT'] = 'Sign out';
$lang['waray']['SIGN_OUT'] = 'Sign out';
$lang['bisaya']['SIGN_OUT'] = 'Sign out';
 

