<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
                
                
            </div>

            <div class="row">
                <div class="col-12 col-xl-8">
                    <div class="card card-body border-0 shadow mb-4">
                        <h2 class="h5 mb-4"><?=$page['title'];?></h2>
                        <?php resultMessage(); ?>
                        <form action="<?=SUBMIT?>" method="post" id="form">
                        <input type="hidden" name="id" value="<?=!empty($_GET['id'])? $_GET['id']:'';?>">
                
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <label for="first_name">First Name</label>
                                        <input class="form-control" name="first_name" id="first_name" type="text" placeholder="Enter your first_name" value="<?=!empty($data['first_name'])? $data['first_name']:'';?>" required>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <label for="last_name">Last Name</label>
                                        <input class="form-control" name="last_name" id="last_name" type="text" placeholder="Also your last_name" value="<?=!empty($data['last_name'])? $data['last_name']:'';?>" required>
                                    </div>
                                </div>
                            </div>


                            <div class="row align-items-center">
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <label for="email">Email Address</label>
                                        <input class="form-control" name="email" id="email" type="email" placeholder="email" value="<?=!empty($data['email'])? $data['email']:'';?>" required>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="gender">School</label>
                                    <select class="form-select mb-0" id="school_id" name="school_id" aria-label="Gender select example">
                                        <option value="">Select School</option>
                                        <?php foreach($schoolArr['body'] as $school): ?>  
                                            <option value="<?=$school['reg_id'];?>" <?=(!empty($data['school_id']) && $data['school_id']==$school['reg_id']) ? 'selected':'';?>><?=$school['school_name'];?></option> 
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>



                            <div class="row align-items-center">
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <label for="password">Password</label>
                                        <input class="form-control" name="password" id="password" type="password" placeholder="Enter password" value="<?=!empty($data['password'])? $data['password']:'';?>" >
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="gender">Status</label>
                                    <select class="form-select mb-0" id="gender" name="status" aria-label="Gender select example">
                                        <option value="">Select Status</option>
                                        <option value="0" <?=(!empty($data['status']) && $data['status']=='0') ? 'selected':'';?>>Inactive</option>
                                        <option value="1" <?=(!empty($data['status']) && $data['status']=='1') ? 'selected':'';?>>Active</option> 
                                    </select>
                                </div>
                            </div>



                            
 
                              
                            <div class="mt-3">
                                <button class="btn btn-primary mt-2 animate-up-2" type="submit"  >Save</button>
                            </div>
                        </form>
                    </div>
                   
                </div>
              
            </div>



            <script>
               
            </script>