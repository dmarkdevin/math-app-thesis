            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
                <div class="d-block mb-4 mb-md-0">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                        <ol class="breadcrumb breadcrumb-dark breadcrumb-transparent">
                            <li class="breadcrumb-item">
                                <a href="#">
                                    <svg class="icon icon-xxs" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"></path></svg>
                                </a>
                            </li>
                            <!-- <li class="breadcrumb-item"><a href="#"></a></li> -->
                            <li class="breadcrumb-item active" aria-current="page">
                                <?=$page['title'];?></li>
                        </ol>
                    </nav>
                    <h2 class="h4">
                        <svg class="icon icon-xs me-2" fill="none" stroke="currentColor" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
                                <path  stroke-linecap="round" stroke-linejoin="round" d="M2.25 21h19.5m-18-18v18m10.5-18v18m6-13.5V21M6.75 6.75h.75m-.75 3h.75m-.75 3h.75m3-6h.75m-.75 3h.75m-.75 3h.75M6.75 21v-3.375c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125V21M3 3h12m-.75 4.5H21m-3.75 3.75h.008v.008h-.008v-.008zm0 3h.008v.008h-.008v-.008zm0 3h.008v.008h-.008v-.008z"></path>
                            </svg><?=$page['title'];?></h2>
                    <!-- <p class="mb-0">Your web analytics dashboard template.</p> -->
                </div>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <a href="<?= addLink(); ?>" class="btn btn-sm btn-gray-800 d-inline-flex align-items-center">
                        <svg class="icon icon-xs me-2" fill="none" stroke="currentColor" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M19 7.5v3m0 0v3m0-3h3m-3 0h-3m-2.25-4.125a3.375 3.375 0 11-6.75 0 3.375 3.375 0 016.75 0zM4 19.235v-.11a6.375 6.375 0 0112.75 0v.109A12.318 12.318 0 0110.374 21c-2.331 0-4.512-.645-6.374-1.766z"></path>
                        </svg>
                        Add New
                    </a>
                   
                </div>
            </div>
            
            <?php resultMessage(); ?>
           
            <div class="card card-body border-0 shadow table-wrapper table-responsive">
                <table class="table table-hover" id="tbl">
                    <thead>
                        <tr>
                            <th class="border-gray-200">School ID</th>
                            <th class="border-gray-200">School Name</th>
                            <th class="border-gray-200">School Address</th>
                            <th class="border-gray-200">Status</th>
                            <th class="border-gray-200">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Item -->
                        <?php foreach($dataArray['body'] as $data): ?>

                        <tr>
                            <td>
                                <a href="#" class="fw-bold"><?= $data['school_id']; ?></a>
                            </td>

                            <td>
                                <span class="fw-normal"><?= $data['school_name']; ?></span>
                            </td>  

                            <td>
                                <span class="fw-normal"><?= $data['school_address']; ?></span>
                            </td>  

                            <td><?= statusText($data['status']); ?></td>

                            <td>
                                <a class="btn btn-sm btn-outline-info" href="edit_<?= $page['label']; ?>.php?id=<?=encrypt_decrypt("encrypt", $data['reg_id']);?>" >
                                    <svg class="icon icon-xs " fill="none" stroke="currentColor" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"></path>
                                    </svg>
                                    Edit
                                </a>
                                <a class="btn btn-sm btn-outline-danger me-1" onclick="return confirm('Are you sure you want to delete this <?= $page['label']; ?> record ?');" href="delete_<?= $page['label']; ?>.php?id=<?=encrypt_decrypt("encrypt", $data['reg_id']);?>">
                                    <svg class="icon icon-xs " fill="none" stroke="currentColor" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"></path>
                                    </svg>
                                    Delete
                                </a>
                            </td>
                        </tr>

                        <?php endforeach; ?>
                                                   
                    </tbody>
                </table>
                
            </div>