<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
                <div class="d-block mb-4 mb-md-0">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                        <ol class="breadcrumb breadcrumb-dark breadcrumb-transparent">
                            <li class="breadcrumb-item">
                                <a href="#">
                                    <svg class="icon icon-xxs" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"></path></svg>
                                </a>
                            </li>
                            <!-- <li class="breadcrumb-item"><a href="#"></a></li> -->
                            <li class="breadcrumb-item active" aria-current="page"><?=$page['title'];?></li>
                        </ol>
                    </nav>
                    <h2 class="h4"><?=$page['title'];?></h2>
                    <!-- <p class="mb-0">Your web analytics dashboard template.</p> -->
                </div>
                <div class="btn-toolbar mb-2 mb-md-0">
                    
                   
                </div>
            </div>

          
            
            <?php resultMessage(); ?>
            
           
            <div class="card card-body border-0 shadow table-wrapper table-responsive">
           
            <div class="btn-toolbar mb-2 mb-md-0 mb-5">
            <a href="<?= SUBMIT;?>" class="btn btn-sm btn-outline-gray-800  bt-5">
             
                <svg  class="icon icon-xs me-2"fill="none" stroke="currentColor" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
                <path stroke-linecap="round" stroke-linejoin="round" d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                </svg>
                SAVE DATA
            </a>
            </div>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th class="border-gray-200">FIRST NAME</th>
                            <th class="border-gray-200">LAST NAME </th>
                            <th class="border-gray-200">STUDENT ID </th> 
                            <th class="border-gray-200">GENDER</th> 
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Item -->
                        <?php foreach($dataArray['body'] as $data): ?>

                        <tr>
                            <td>
                                <a href="#" class="fw-bold"><?= $data[0]; ?></a> 
                            </td> 
                            <td><a href="#" class="fw-bold"><?= $data[1]; ?></a></td>
                            <td><a href="#" class="fw-bold"><?= $data[2]; ?></a></td> 
                            <td><a href="#" class="fw-bold"><?= $data[3]; ?></a></td> 
                        </tr>

                        <?php endforeach; ?>
                                                   
                    </tbody>
                </table>
                
            </div>