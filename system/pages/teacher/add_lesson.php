<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
                
                
            </div>

            <div class="row">
                <div class="col-12 col-xl-8">
                    <div class="card card-body border-0 shadow mb-4">
                        <h2 class="h5 mb-4"><?=$page['title'];?></h2>
                        <?php resultMessage(); ?>
                        <form action="<?=SUBMIT?>" method="post">
                        <input type="hidden" name="id" value="<?=!empty($_GET['id'])? $_GET['id']:'';?>">
                
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <label for="lesson_title">Lesson</label>
                                        <input class="form-control" name="lesson_title" id="lesson_title" type="text" placeholder="Enter your lesson title" value="<?=!empty($data['lesson_title'])? $data['lesson_title']:'';?>" required>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="gender">Module</label>
                                    <select class="form-select mb-0" id="module_id" name="module_id" aria-label="" required>
                                        <option value="">Select Module</option>
                                        <?php foreach($moduleArr['body'] as $module): ?>  
                                            <option value="<?=$module['reg_id'];?>" <?=(!empty($data['module_id']) && $data['module_id']==$module['reg_id']) ? 'selected':'';?>><?=$module['category_name'].' - '.$module['module_title'];?></option> 
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row align-items-center">
                               
                                <div class="col-md-6 mb-3">
                                    <label for="gender">Status</label>
                                    <select class="form-select mb-0" name="status" id="status" name="status" aria-label="" required>
                                        <option value="">Select Status</option>
                                        <option value="0" <?=(!empty($data['status']) && $data['status']=='0') ? 'selected':'';?>>Inactive</option>
                                        <option value="1" <?=(!empty($data['status']) && $data['status']=='1') ? 'selected':'';?>>Active</option> 
                                    </select>
                                </div>
                            </div> 

                            <div class="mt-3">
                                <button class="btn btn-primary mt-2 animate-up-2" type="submit">Save</button>
                            </div>
                        </form>
                    </div>
                   
                </div>
              
            </div>