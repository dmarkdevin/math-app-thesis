<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
                
                
            </div>

            <div class="row">
                <div class="col-12 col-xl-8">
                    <div class="card card-body border-0 shadow mb-4">
                        <h2 class="h5 mb-4"><?=$page['title'];?></h2>
                        <?php resultMessage(); ?>
                        <form action="<?=SUBMIT?>" method="post">
                        <input type="hidden" name="id" value="<?=!empty($_GET['id'])? $_GET['id']:'';?>">
                
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <label for="module_title">Module</label>
                                        <input class="form-control" name="module_title" id="module_title" type="text" placeholder="Enter your module title" value="<?=!empty($data['module_title'])? $data['module_title']:'';?>" required>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="gender">Quarter</label>
                                    <select class="form-select mb-0" id="category_id" name="category_id" aria-label="" required>
                                        <option value="">Select Quarter</option>
                                        <?php foreach($categoryArr['body'] as $category): ?>  
                                            <option value="<?=$category['id'];?>" <?=(!empty($data['category_id']) && $data['category_id']==$category['id']) ? 'selected':'';?>><?=$category['category_name'];?></option> 
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row align-items-center">
                               
                                <div class="col-md-6 mb-3">
                                    <label for="gender">Status</label>
                                    <select class="form-select mb-0" name="status" id="status" name="status" aria-label="" required>
                                        <option value="">Select Status</option>      
                                        <option value="0" <?=(!empty($data['status']) && $data['status']=='0') ? 'selected':'';?>>Inactive</option>
                                        <option value="1" <?=(!empty($data['status']) && $data['status']=='1') ? 'selected':'';?>>Active</option> 
                                    </select>
                                </div>
                            </div> 

                            <div class="mt-3">
                                <button class="btn btn-primary mt-2 animate-up-2" type="submit">Save</button>
                            </div>
                        </form>
                    </div>
                   
                </div>
              
            </div>