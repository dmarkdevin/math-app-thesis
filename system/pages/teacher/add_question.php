<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
                
                
            </div>

            <div class="row">
                <div class="col-12 col-xl-8">
                    <div class="card card-body border-0 shadow mb-4">
                        <h2 class="h5 mb-4"><?=$page['title'];?></h2>
                        <?php resultMessage(); ?>
                        <form action="<?=SUBMIT?>" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="id" value="<?=!empty($_GET['id'])? $_GET['id']:'';?>">
                
                            
                        
                            <div class="row">
                                <div class="col-md-15 mb-2">
                                      <div class="my-4">
                                        <label for="question">Question</label>
                                        <textarea class="form-control" placeholder="Add your questions" name="question" id="question" type="text" rows="4"   required><?=!empty($data['question'])? $data['question']:'';?></textarea>
                                      </div>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="gender">Lesson</label>
                                    <select class="form-select mb-0" id="lesson_id" name="lesson_id" aria-label="">
                                        <option value="">Select Lesson</option>
                                        <?php foreach($lessonArr['body'] as $lesson): ?>  
                                            <option value="<?=$lesson['reg_id'];?>" <?=(!empty($data['lesson_id']) && $data['lesson_id']==$lesson['reg_id']) ? 'selected':'';?>><?=str_replace("Quarter","Q",$lesson['category_name'] ).' - '. $lesson['module_title'].' - '.$lesson['lesson_title'];?></option> 
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <label for="option1">Option1</label>
                                        <input class="form-control" name="option1" id="option1" type="text" placeholder="" value="<?=!empty($data['option1'])? $data['option1']:'';?>" required>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <label for="option2">Option2</label>
                                        <input class="form-control" name="option2" id="option2" type="text" placeholder="" value="<?=!empty($data['option2'])? $data['option2']:'';?>" required>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <label for="option3">Option3</label>
                                        <input class="form-control" name="option3" id="option3" type="text" placeholder="" value="<?=!empty($data['option3'])? $data['option3']:'';?>" >
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <label for="option4">Option4</label>
                                        <input class="form-control" name="option4" id="option4" type="text" placeholder="" value="<?=!empty($data['option4'])? $data['option4']:'';?>" >
                                    </div>
                                </div>
                            </div>


                            <div class="row align-items-center">
                                <div class="col-md-6 mb-3">
                                    <label for="answer">Answer</label>
                                    <select class="form-select mb-0" name="answer" id="answer"  aria-label="">
                                        <option value="" >Select Answer</option>
                                        <option value="1" <?=(!empty($data['answer']) && $data['answer']=='1') ? 'selected':'';?>>Option 1</option>
                                        <option value="2" <?=(!empty($data['answer']) && $data['answer']=='2') ? 'selected':'';?>>Option 2</option> 
                                        <option value="3" <?=(!empty($data['answer']) && $data['answer']=='3') ? 'selected':'';?>>Option 3</option> 
                                        <option value="4" <?=(!empty($data['answer']) && $data['answer']=='4') ? 'selected':'';?>>Option 4</option> 
                                    </select>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="gender">Status</label>
                                    <select class="form-select mb-0" name="status" id="status" name="status" aria-label="">
                                        <option value="">Select Status</option>
                                        <option value="0" <?=(!empty($data['status']) && $data['status']=='0') ? 'selected':'';?>>Inactive</option>
                                        <option value="1" <?=(!empty($data['status']) && $data['status']=='1') ? 'selected':'';?>>Active</option> 
                                    </select>
                                </div>
                            </div> 

                            <div class="row align-items-center">
                                <div class="col-md-6 mb-3">
                                    <label for="formFile" class="form-label">Upload Image</label>
                                    <input class="form-control" type="file" id="UploadFile" name="UploadFile">
                                </div>
                                <div class="col-md-6 mb-3">
                                    
                                </div>
                            </div> 


                            
                      

                            <div class="mt-3">
                                <button class="btn btn-primary mt-2 animate-up-2" type="submit">Save</button>
                            </div>
                        </form>
                    </div>
                   
                </div>
              
                <?php if (!empty($data['imagefile'])) {
                    if (file_exists(DOCUMENT_ROOT . 'uploads/' . $data['imagefile']) && !empty($data['imagefile'])): ?>
                <div class="col-12 col-xl-4">
                    <div class="row">
                        
                        <div class="col-12 mb-4">
                            <div class="card shadow border-0 text-center p-0">
                                
                                <div class="card-body pb-5">
                                    <img src="<?= BASEPATH; ?>uploads/<?= $data['imagefile']; ?>" class="" alt="<?= $data['imagefile']; ?>">  
                                </div>
                            </div>

                        </div>
 
                    </div>
                </div>
                <?php endif;
                } ?>


            </div>