<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
                
                
            </div>

            <div class="row">
                <div class="col-12 col-xl-8">
                    <div class="card card-body border-0 shadow mb-4">
                        <h2 class="h5 mb-4"><?=$page['title'];?></h2>
                        <?php resultMessage(); ?>
                        <form action="<?=SUBMIT?>" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="id" value="<?=!empty($_GET['id'])? $_GET['id']:'';?>">
                
                            
                        
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <label for="question">Number1</label>
                                        <input class="form-control" name="number1" id="number1" type="number" placeholder="number1" value="<?=!empty($data['number1'])? $data['number1']:'';?>" required>
                                    </div>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <div>
                                        <label for="question">Number2</label>
                                        <input class="form-control" name="number2" id="number2" type="number" placeholder="number1" value="<?=!empty($data['number2'])? $data['number2']:'';?>" required>
                                    </div>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="gender">Operator</label>
                                    <select class="form-select mb-0" id="operator" name="operator" aria-label="">
                                        <option value="">Select Operator</option>

                                        <option value="1" <?=(!empty($data['operator']) && $data['operator']==1) ? 'selected':'';?>>ADDITION</option> 
                                        <option value="2" <?=(!empty($data['operator']) && $data['operator']==2) ? 'selected':'';?>>SUBSTRACTION</option> 
                                        <option value="3" <?=(!empty($data['operator']) && $data['operator']==3) ? 'selected':'';?>>MULTIPLICATION</option> 
                                        <option value="4" <?=(!empty($data['operator']) && $data['operator']==4) ? 'selected':'';?>>DIVISION</option> 
                                      

                                    </select>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <div>
                                        <label for="answer">Answer</label>
                                        <input class="form-control" name="answer" id="answer" type="number" placeholder="" value="<?=!empty($data['answer'])? $data['answer']:'';?>" required>
                                    </div>
                                </div>
                                
                            </div>


                            <div class="row">
                                

                                 
                                <div class="col-md-6 mb-3">
                                    <label for="gender">Status</label>
                                    <select class="form-select mb-0" name="status" id="status" name="status" aria-label="">
                                        <option value="">Select Status</option>
                                        <option value="0" <?=(!empty($data['status']) && $data['status']=='0') ? 'selected':'';?>>Inactive</option>
                                        <option value="1" <?=(!empty($data['status']) && $data['status']=='1') ? 'selected':'';?>>Active</option> 
                                    </select>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="gender">Numeracy</label>
                                    <select class="form-select mb-0" name="numeracy" id="numeracy" aria-label="">
                                        <option value="1" <?=(!empty($data['numeracy']) && $data['numeracy']=='1') ? 'selected':'';?>>1st Numeracy</option>
                                        <option value="2" <?=(!empty($data['numeracy']) && $data['numeracy']=='2') ? 'selected':'';?>>2nd Numeracy</option> 
                                    </select>
                                </div>
                            </div>

 
 

                            
                      

                            <div class="mt-3">
                                <button class="btn btn-primary mt-2 animate-up-2" type="submit">Save</button>
                            </div>
                        </form>
                    </div>
                   
                </div>
              
                <?php if (!empty($data['imagefile'])) {
                    if (file_exists(DOCUMENT_ROOT . 'uploads/' . $data['imagefile']) && !empty($data['imagefile'])): ?>
                <div class="col-12 col-xl-4">
                    <div class="row">
                        
                        <div class="col-12 mb-4">
                            <div class="card shadow border-0 text-center p-0">
                                
                                <div class="card-body pb-5">
                                    <img src="<?= BASEPATH; ?>uploads/<?= $data['imagefile']; ?>" class="" alt="<?= $data['imagefile']; ?>">  
                                </div>
                            </div>

                        </div>
 
                    </div>
                </div>
                <?php endif;
                } ?>


            </div>