<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Primary Meta Tags -->
<title>MathApp3.com</title>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="title" content="MathApp3">
 
 

<!-- Favicon -->
<link rel="apple-touch-icon" sizes="120x120" href="<?=THEME1;?>assets/img/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?=THEME1;?>assets/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?=THEME1;?>assets/img/favicon/favicon-16x16.png">
<link rel="manifest" href="<?=THEME1;?>assets/img/favicon/site.webmanifest">
<link rel="mask-icon" href="<?=THEME1;?>assets/img/favicon/safari-pinned-tab.svg" color="#ffffff">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">

<!-- Fontawesome -->
<link type="text/css" href="<?=THEME1;?>vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">

<!-- Pixel CSS -->
<link type="text/css" href="<?=THEME1;?>css/pixel.css" rel="stylesheet">

<style>
body {
  /* background-image: url(<?=THEME1;?>assets/img/pages/form-image2.jpeg);
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-position: center;
  background-size:     cover;        */
}
</style>

</head>

<body>
    <main>
   
       
        <section class="min-vh-100 d-flex align-items-center section-image overlay-soft-dark" data-background="">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="signin-inner my-4 my-lg-0 bg-white shadow-soft border rounded border-gray-300 p-4 p-lg-5 w-100 fmxw-500">
                            <div class="text-center text-md-center mb-4 mt-md-0">
                                <h1 class="mb-0 h3">Enter your section code</h1>
                            </div>
                            <?php resultMessage(); ?>
                            <form action="<?=SUBMIT?>" method="post" class="mt-4">
                                <!-- Form -->
                                <div class="form-group mb-4">
                                    <label for="section_code">Section Code</label>
                                    <div class="input-group">
                                         
                                        <input type="text" class="form-control" placeholder="Enter your section code" id="code" name="code" required>
                                    </div>  
                                </div>
                                

                                <div class="d-grid">
                                    <button type="submit" class="btn btn-primary">CONTINUE</button>
                                </div>
                            </form>
                          
                          
                            <div class="d-flex justify-content-center align-items-center mt-4">
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
 

    </main>

    <!-- Core -->
<script src="<?=THEME1;?>vendor/@popperjs/core/dist/umd/popper.min.js"></script>
<script src="<?=THEME1;?>vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?=THEME1;?>vendor/headroom.js/dist/headroom.min.js"></script>

<!-- Vendor JS -->
<script src="<?=THEME1;?>vendor/onscreen/dist/on-screen.umd.min.js"></script>
<script src="<?=THEME1;?>vendor/jarallax/dist/jarallax.min.js"></script>
<script src="<?=THEME1;?>vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>
<script src="<?=THEME1;?>vendor/vivus/dist/vivus.min.js"></script>
<script src="<?=THEME1;?>vendor/vanillajs-datepicker/dist/js/datepicker.min.js"></script>

<script async defer src="https://buttons.github.io/buttons.js"></script>

<!-- Pixel JS -->
<script src="<?=THEME1;?>assets/js/pixel.js"></script>

</body>

</html>