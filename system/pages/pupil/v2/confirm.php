<?php $language = language(); ?>

<!DOCTYPE html>
<html lang="en">

<head> 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Primary Meta Tags -->
<title></title>
<!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
<meta name="viewport" 
      content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
 
<!-- Favicon -->
<link rel="apple-touch-icon" sizes="120x120" href="<?=THEME2;?>assets/img/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?=THEME2;?>assets/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?=THEME2;?>assets/img/favicon/favicon-16x16.png">
<link rel="manifest" href="<?=THEME2;?>assets/img/favicon/site.webmanifest">
<link rel="mask-icon" href="<?=THEME2;?>assets/img/favicon/safari-pinned-tab.svg" color="#ffffff">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">

<!-- Sweet Alert -->
<link type="text/css" href="<?=THEME2;?>vendor/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">

<!-- Notyf -->
<link type="text/css" href="<?=THEME2;?>vendor/notyf/notyf.min.css" rel="stylesheet">

<!-- Volt CSS -->
<link type="text/css" href="<?=THEME2;?>css/volt.css" rel="stylesheet">

<link type="text/css" href="<?=THEME1;?>vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
 
<!-- NOTICE: You can use the _analytics.html partial to include production code specific code & trackers -->

</head>

<body>

    <!-- NOTICE: You can use the _analytics.html partial to include production code specific code & trackers -->
    

    <main>

        <!-- Section -->
        <section class="vh-lg-100 mt-5 mt-lg-0 bg-soft d-flex align-items-center">
            <div class="container">


              

                <div class="row justify-content-center form-bg-image " >
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class=" border-0 rounded p-4 p-lg-5 w-100 fmxw-500 ">

                        <div class="avatar avatar-lg mx-auto mb-3">
                                    <!-- <img class="rounded-circle" alt="Image placeholder" src="../../assets/img/team/profile-picture-3.jpg"> -->

                                    <img src=" <?= avatar($_SESSION['mathapp']['prelogin']['avatar']);?>" class="mb-2 rounded-circleX img-thumbnailX image-lgX border-secondaryX p-2X" alt="Joseph Avatar">

                                </div>
                           
                            <div class="text-center text-black border-topX border-bottomX border-gray-300X my-6X py-6X">
                            <h4 class="h4 mb-5">
                                <span class="me-1">
                                <!-- <i class="far fa-newspaper"></i> -->
                                </span> <?= $lang[$language]['IS_IT_YOU'];?> <b><?= $_SESSION['mathapp']['prelogin']['account_name'];?></b>?
                            </h4>
                            
                          

                            <a href="<?= SUBMIT;?>" type="button" class="btn btn-success mb-2 mx-1 animate-up-2 text-white">
                                <span class="me-2">
                                <i class="far fa-thumbs-up"></i>
                                </span><?= $lang[$language]['YES'];?> </a>

                            <a href="login.php" type="button" class="btn btn-danger mb-2 mx-1 animate-up-2">
                                <span class="me-2">
                                <i class="fas fa-times"></i>
                                </span><?= $lang[$language]['NO'];?> </a>


                       
                            </div>



                           
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <style>
    @import url("https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap");

* {
  margin: 0;
  padding: 0;
}
 
 
 
body {
  /* background: #ecf2fe; */
  /* height: 100vh; */

  background-image: url(<?=THEME1;?>assets/img/pages/bg6.png);
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-position: center;
  background-size:     cover;      

}
    

.plans {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
  -ms-flex-pack: justify;
  justify-content: space-between;

  max-width: 970px;
  padding: 85px 50px;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  /* background: #fff; */
  background: transparent;
  
  border-radius: 20px;
  /* -webkit-box-shadow: 0px 8px 10px 0px #d8dfeb; */
  /* box-shadow: 0px 8px 10px 0px #d8dfeb; */
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
}

.plans .plan input[type="radio"] {
  position: absolute;
  opacity: 0;
}

.plans .plan {
  cursor: pointer;
  width: 48.5%;
}

.plans .plan .plan-content {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  padding: 30px;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  border: 2px solid #e1e2e7;
  border-radius: 10px;
  -webkit-transition: -webkit-box-shadow 0.4s;
  transition: -webkit-box-shadow 0.4s;
  -o-transition: box-shadow 0.4s;
  transition: box-shadow 0.4s;
  transition: box-shadow 0.4s, -webkit-box-shadow 0.4s;
  position: relative;
}

.plans .plan .plan-content img {
  margin-right: 30px;
  height: 72px;
}

.plans .plan .plan-details span {
  margin-bottom: 10px;
  display: block;
  font-size: 20px;
  line-height: 24px;
  color: #252f42;
}

.container .title {
  font-size: 16px;
  font-weight: 500;
  -ms-flex-preferred-size: 100%;
  flex-basis: 100%;
  color: #252f42;
  margin-bottom: 20px;
}

.plans .plan .plan-details p {
  color: #646a79;
  font-size: 14px;
  line-height: 18px;
}

.plans .plan .plan-content:hover {
  -webkit-box-shadow: 0px 3px 5px 0px #e8e8e8;
  box-shadow: 0px 3px 5px 0px #e8e8e8;
}

.plans .plan input[type="radio"]:checked + .plan-content:after {
  content: "";
  position: absolute;
  height: 8px;
  width: 8px;
  background: #216fe0;
  right: 20px;
  top: 20px;
  border-radius: 100%;
  border: 3px solid #fff;
  -webkit-box-shadow: 0px 0px 0px 2px #0066ff;
  box-shadow: 0px 0px 0px 2px #0066ff;
}

.plans .plan input[type="radio"]:checked + .plan-content {
  border: 2px solid #216ee0;
  background: #eaf1fe;
  -webkit-transition: ease-in 0.3s;
  -o-transition: ease-in 0.3s;
  transition: ease-in 0.3s;
}

@media screen and (max-width: 991px) {
  .plans {
    margin: 0 20px;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-align: start;
    -ms-flex-align: start;
    align-items: flex-start;
    padding: 40px;
  }

  .plans .plan {
    width: 100%;
  }

  .plan.complete-plan {
    margin-top: 20px;
  }

  .plans .plan .plan-content .plan-details {
    width: 70%;
    display: inline-block;
  }

  .plans .plan input[type="radio"]:checked + .plan-content:after {
    top: 45%;
    -webkit-transform: translate(-50%);
    -ms-transform: translate(-50%);
    transform: translate(-50%);
  }
}

@media screen and (max-width: 767px) {
  .plans .plan .plan-content .plan-details {
    width: 60%;
    display: inline-block;
  }
}

@media screen and (max-width: 540px) {
  .plans .plan .plan-content img {
    margin-bottom: 20px;
    height: 56px;
    -webkit-transition: height 0.4s;
    -o-transition: height 0.4s;
    transition: height 0.4s;
  }

  .plans .plan input[type="radio"]:checked + .plan-content:after {
    top: 20px;
    right: 10px;
  }

  .plans .plan .plan-content .plan-details {
    width: 100%;
  }

  .plans .plan .plan-content {
    padding: 20px;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-align: baseline;
    -ms-flex-align: baseline;
    align-items: baseline;
  }
}

/* inspiration */
.inspiration {
  font-size: 12px;
  margin-top: 50px;
  position: absolute;
  bottom: 10px;
  font-weight: 300;
}

.inspiration a {
  color: #666;
}
@media screen and (max-width: 767px) {
  /* inspiration */
  .inspiration {
    display: none;
  }
}

.mb-3{
    margin-bottom:30px;
}

    </style>
   

    <!-- Core -->
<script src="<?=THEME2;?>vendor/@popperjs/core/dist/umd/popper.min.js"></script>
<script src="<?=THEME2;?>vendor/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Vendor JS -->
<script src="<?=THEME2;?>vendor/onscreen/dist/on-screen.umd.min.js"></script>

<!-- Slider -->
<script src="<?=THEME2;?>vendor/nouislider/distribute/nouislider.min.js"></script>

<!-- Smooth scroll -->
<script src="<?=THEME2;?>vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>

<!-- Charts -->
<script src="<?=THEME2;?>vendor/chartist/dist/chartist.min.js"></script>
<script src="<?=THEME2;?>vendor/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>

<!-- Datepicker -->
<script src="<?=THEME2;?>vendor/vanillajs-datepicker/dist/js/datepicker.min.js"></script>

<!-- Sweet Alerts 2 -->
<script src="<?=THEME2;?>vendor/sweetalert2/dist/sweetalert2.all.min.js"></script>

<!-- Moment JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>

<!-- Vanilla JS Datepicker -->
<script src="<?=THEME2;?>vendor/vanillajs-datepicker/dist/js/datepicker.min.js"></script>

<!-- Notyf -->
<script src="<?=THEME2;?>vendor/notyf/notyf.min.js"></script>

<!-- Simplebar -->
<script src="<?=THEME2;?>vendor/simplebar/dist/simplebar.min.js"></script>

<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>

<!-- Volt JS -->
<script src="<?=THEME2;?>assets/js/volt.js"></script>

    
</body>

</html>
