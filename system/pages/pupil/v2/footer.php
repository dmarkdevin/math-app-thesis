
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/@popperjs/core/dist/umd/popper.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/headroom.js/dist/headroom.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/onscreen/dist/on-screen.umd.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/nouislider/distribute/nouislider.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/jarallax/dist/jarallax.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/vivus/dist/vivus.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/chartist/dist/chartist.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/@glidejs/glide/dist/glide.min.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/countup.js/dist/countUp.umd.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/vendor/vanillajs-datepicker/dist/js/datepicker.min.js"></script>
    <script async defer="defer" src="https://buttons.github.io/buttons.js"></script>
    <script src="https://demo.themesberg.com/pixel-pro/v5/assets/js/pixel.js"></script>
     
   
  </body>
</html>