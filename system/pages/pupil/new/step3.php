<main>
     
      <section class="section-header text-black pb-10 pb-sm-8 pb-lg-11">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-12 col-md-8 text-center">
              <h1 class="display-2 mb-4">Select Lesson</h1>
              <p class="lead">Select Lesson</p>
            </div>
          </div>
        </div>
      </section>
      <section class="section section-lg line-bottom-light">
        <div class="container mt-n10 mt-lg-n12 z-2">
          <div class="row">
             

          <?php
                                foreach($dataArray["body"] as $data){
                                    $mid = encrypt_decrypt('encrypt', $data['reg_id']);

                                    ?>


            <div class="col-12 col-md-6 col-lg-4 mb-4 mb-lg-5">
              <div class="card shadow bg-white border-gray-300 p-4 rounded">
                <a href="quiz.php?lid=<?= $mid; ?>">
                <!-- https://demo.themesberg.com/pixel-pro/v5/assets/img/blog/image-2.jpg -->
                  <img src="<?= BASEPATH;?>assets/images/lessonbg.jpg" class="card-img-top rounded" alt="our desk">
                </a>
                <div class="card-body p-0 pt-4">
                  <a href="quiz.php?lid=<?= $mid; ?>" class="h3"><?= $data['lesson_title'];?></a>
                  <!-- <div class="d-flex align-items-center my-3">
                    <img class="avatar avatar-sm rounded-circle" src="https://demo.themesberg.com/pixel-pro/v5/assets/img/team/profile-picture-2.jpg" alt="Neil avatar">
                    <h3 class="h6 small ms-2 mb-0">Neil Sims Curran</h3>
                  </div> -->
                  <!-- <p class="mb-0">Richard Thomas was born in 1990, and at only 29 years old, his trajectory is good. When he is asked how he describes himself, he responds, "designer but in a broad sense". His projects?</p> -->
                </div>
              </div>
            </div>

            <?php   }
                                ?>

 
          </div>
        </div>
      </section>
    </main>
    
  <style>


@import url("https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap");

* {
  margin: 0;
  padding: 0;
}
        body {
 
  background-image: url(<?=THEME1;?>assets/img/pages/bg11.jpg);
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-position: center;
  background-size:     cover;      


}

.main-ui {
  max-width: 800px;
  margin: 0 auto;
}

.our-form {
  display: flex;
  justify-content: center;
}

.status {
  text-align: center;
  font-size: .85rem;
}

.boxes {
  display: flex;
  width: 100%;
}

.progress {
  border: 1px solid #c7c7c7;
  border-right: none;
  position: relative;
}

.progress-inner {
  position: absolute;
  top: 0;
  bottom: 0;
  width: 100%;
  background-color: #7ecc00;
  opacity: .57;
  transform: scaleX(0);
  transform-origin: center left;
  transition: transform .4s ease-out;
}

.box {
  height: 40px;
  border-right: 1px solid #c7c7c7;
  flex: 1;
}

.overlay {
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: rgba(255, 255, 255, .82);
  display: flex;
  justify-content: center;
  align-items: center;
  opacity: 0;
  visibility: hidden;
  transition: all .33s ease-out;
  transform: scale(1.2);
}

.overlay-inner {
  text-align: center;
  max-width: 700px;
    
    
    