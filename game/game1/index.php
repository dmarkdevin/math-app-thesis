<?php
require_once '../../global.php';
?>

<html>

<head>
<meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
<link rel="apple-touch-icon" sizes="120x120" href="<?=THEME2;?>assets/img/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?=THEME2;?>assets/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?=THEME2;?>assets/img/favicon/favicon-16x16.png">
<link rel="manifest" href="<?=THEME2;?>assets/img/favicon/site.webmanifest">
<link rel="mask-icon" href="<?=THEME2;?>assets/img/favicon/safari-pinned-tab.svg" color="#ffffff">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">
<script src="<?=BASEPATH;?>assets/js/jquery.min.js"></script>
<link href="<?=BASEPATH;?>assets/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

        <section class="section" id="pricing">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-12">
                        
                    
                    
             

<audio id="myAudio">
 
 <source src="<?= BASEPATH;?>assets/audio/Congratulations_audio.mp3" type="audio/mpeg">
 Your browser does not support the audio element.
</audio>



<div class="main-ui">
 <p class="problem"></p>

 <form action="" class="our-form">
   <input type="number" class="our-field" autocomplete="off" id="our-field">
   <button>Submit</button>
 </form>

 <p class="status">You need <span class="points-needed">10</span> more points, and are allowed to make <span class="mistakes-allowed">2</span> more mistakes.</p>

 <div class="progress">
   <div class="boxes">
     <div class="box"></div>
     <div class="box"></div>
     <div class="box"></div>
     <div class="box"></div>
     <div class="box"></div>
     <div class="box"></div>
     <div class="box"></div>
     <div class="box"></div>
     <div class="box"></div>
     <div class="box"></div>
   </div>
   <div class="progress-inner"></div>
 </div>

</div>

<div class="overlay">
 <div class="overlay-inner">
   <p class="end-message"></p>
   <button class="reset-button me-2">Start Over</button>   <br><br>   <a href="<?=BASEPATH;?>p/" >Exit</a>
 </div>
</div>

<script>
const problemElement = document.querySelector(".problem")
const ourForm = document.querySelector(".our-form")
const ourField = document.querySelector(".our-field")
const pointsNeeded = document.querySelector(".points-needed")
const mistakesAllowed = document.querySelector(".mistakes-allowed")
const progressBar = document.querySelector(".progress-inner")
const endMessage = document.querySelector(".end-message")
const resetButton = document.querySelector(".reset-button")

let state = {
 score: 0,
 wrongAnswers: 0
}

function updateProblem() {
 state.currentProblem = generateProblem()
 problemElement.innerHTML = `${state.currentProblem.numberOne} ${state.currentProblem.operator} ${state.currentProblem.numberTwo}`
 ourField.value = ""
 ourField.focus()
}

updateProblem()

function generateNumber(max) {
 return Math.floor(Math.random() * (max + 1))
}

function generateProblem() {
 return {
   numberOne: generateNumber(10),
   numberTwo: generateNumber(10),
   operator: ['+', '-', 'x'][generateNumber(2)]
 }
}

ourForm.addEventListener("submit", handleSubmit)

function handleSubmit(e) {
 e.preventDefault()

 let correctAnswer
 const p = state.currentProblem
 if (p.operator == "+") correctAnswer = p.numberOne + p.numberTwo
 if (p.operator == "-") correctAnswer = p.numberOne - p.numberTwo
 if (p.operator == "x") correctAnswer = p.numberOne * p.numberTwo

 if (parseInt(ourField.value, 10) === correctAnswer) {
   state.score++
   pointsNeeded.textContent = 10 - state.score
   updateProblem()
   renderProgressBar()
 } else {
   ourField.value = ""
   state.wrongAnswers++
   mistakesAllowed.textContent = 2 - state.wrongAnswers
   problemElement.classList.add("animate-wrong")
   setTimeout(() => problemElement.classList.remove("animate-wrong"), 451)
 }
 checkLogic()
}

function checkLogic() {

 var x = document.getElementById("myAudio"); 

 // if you won
 if (state.score === 10) {
   endMessage.textContent = "Congratulation ! You won."
   x.play(); 
   document.body.classList.add("overlay-is-open")
   setTimeout(() => resetButton.focus(), 331)
 }

 // if you lost
 if (state.wrongAnswers === 3) {
   endMessage.textContent = "Sorry! You lost."
   document.body.classList.add("overlay-is-open")
   setTimeout(() => resetButton.focus(), 331)
 }
}

resetButton.addEventListener("click", resetGame)

function resetGame() {
 document.body.classList.remove("overlay-is-open")
 updateProblem()
 state.score = 0
 state.wrongAnswers = 0
 pointsNeeded.textContent = 10
 mistakesAllowed.textContent = 2
 renderProgressBar()
}

function renderProgressBar() {
 progressBar.style.transform = `scaleX(${state.score / 10})`
}

</script>



                    </div>
                  
                </div> 
            </div>
        </section>
        
        
        
        
        
        
         
    <style>


@import url("https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap");

* {
  margin: 2px;
  padding: 20px;
}
        body {
 
  background-image: url(<?=THEME1;?>assets/img/pages/quiz2.jpg);
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-position: center;
  background-size:     cover;      


}

.main-ui {
  max-width: 800px;
  margin: 0 auto;
}

.our-form {
  display: flex;
  justify-content: center;
}

.status {
  text-align: center;
  font-size: .85rem;
}

.boxes {
  display: flex;
  width: 100%;
}

.progress {
  border: 1px solid #c7c7c7;
  border-right: none;
  position: relative;
}

.progress-inner {
  position: absolute;
  top: 0;
  bottom: 0;
  width: 100%;
  background-color: #7ecc00;
  opacity: .57;
  transform: scaleX(0);
  transform-origin: center left;
  transition: transform .4s ease-out;
}

.box {
  height: 40px;
  border-right: 1px solid #c7c7c7;
  flex: 1;
}

.overlay {
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: rgba(255, 255, 255, .82);
  display: flex;
  justify-content: center;
  align-items: center;
  opacity: 0;
  visibility: hidden;
  transition: all .33s ease-out;
  transform: scale(1.2);
}

.overlay-inner {
  text-align: center;
  max-width: 700px;
}

body.overlay-is-open .overlay,
.overlay--visible {
  opacity: 1;
  visibility: visible;
  transform: scale(1);
}

body.overlay-is-open .main-ui,
.blurred {
  filter: blur(4px);
}

@keyframes showError {
  50% {
    color: red;
    transform: scale(1.2);
  }

  100% {
    color: #333;
    transform: scale(1);
  }
}

.animate-wrong {
  animation: .45s showError;
}

.problem {
  font-size: 5rem;
  margin: 0;
  text-align: center;
}

.end-message {
  font-size: 1.5rem;
  margin-top: 0;
}

.reset-button {
  font-size: 1.2rem;
  background-color: #004094;
  border: none;
  color: #FFF;
  border-radius: 7px;
  padding: 12px 20px;
  display: inline-block;
  outline: none;
  cursor: pointer;
}

.reset-button:hover {
  background-color: #00367e;
}


        </style>


</body>
</html>