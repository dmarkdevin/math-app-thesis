<?php
require_once '../../global.php'; 

$main_page = 'auth.php';

if(empty($_SESSION['mathapp']['prelogin']['pupil'])){
    header('location: '.$main_page);
    exit;
}

if(empty($_SESSION['mathapp']['prelogin']['account_name'])){
    header('location: '.$main_page);
    exit;
}
 
require_once DOCUMENT_ROOT . 'system/pages/pupil/v2/confirm.php';