<?php
require_once '../../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/pupils.php'; 
require_once DOCUMENT_ROOT.'system/classes/sections.php'; 

$main_page = 'school.php';

if (empty($_SESSION['mathapp']['login']['sid'])) {
    $_SESSION['mathapp']['error'] = 'Please enter your section code'; 
    header('location: '.$main_page);
    exit;
}

$database = new Database();
$db = $database->getConnection();

$section = new Sections($db);
$section->reg_id = $_SESSION['mathapp']['login']['sid'];  
$section->getSingle(); 

if($section->reg_id == null){
    echo $_SESSION['mathapp']['error'] = 'wrong section code';
    header('location: '.$main_page);
    exit;
}  

if($section->status != 1){
    echo $_SESSION['mathapp']['error'] = 'invalid request';
    header('location: '.$main_page);
    exit;
}

$pupil = new Pupils($db);

$pupil->section_code = $section->section_code;

$stmt = $pupil->getBySectionCode();  
$dataCount = $stmt->rowCount();

$dataArray = array();
$dataArray["body"] = array();
$dataArray["itemCount"] = $dataCount;

if ($dataCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "first_name" => $first_name,  
            "last_name" => $last_name,  
            "student_id" => $student_id,  
            "username" => $username,  
            "status" => $status,
            "section_name" => $section_name,
            "avatar" => $avatar
        );
        
        array_push($dataArray["body"], $e);
    } 
}

// pr($dataArray);


// foreach($dataArray["body"] as $data){
//     echo '<a href="auth.php?pid='.encrypt_decrypt('encrypt',$data['reg_id']).'">'.$data['first_name'].'</a><br>';
// }



require_once DOCUMENT_ROOT . 'system/pages/pupil/v2/header.php';
require_once DOCUMENT_ROOT . 'system/pages/pupil/v2/login.php';
require_once DOCUMENT_ROOT . 'system/pages/pupil/v2/footer.php';