<?php
require_once '../../global.php';

require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/schools.php';  
require_once DOCUMENT_ROOT.'system/classes/sections.php';  
require_once DOCUMENT_ROOT.'system/classes/teachers.php';  

$label = 'section';
$main_page = 'school.php';

if(empty($_GET['cid'])){
    header('location:'.$main_page);
    exit;
}

$decryptedID = encrypt_decrypt('decrypt', $_GET['cid']);

$database = new Database();
$db = $database->getConnection();

$$label = new Sections($db);
$$label->reg_id = $decryptedID;  

$$label->getSingle();

if($$label->reg_id == null){
    header('location: '.$main_page);
    exit;
}

$data = array(
    "reg_id" =>  $$label->reg_id,
    "teacher_reg_id" => $$label->teacher_reg_id  
);


// pr($data); 





$teacher = new Teachers($db);
$teacher->reg_id = $data['teacher_reg_id'];  

$teacher->getSingle();

if($teacher->reg_id == null){
    header('location: '.$main_page);
    exit;
}

$data2 = array(
    "reg_id" =>  $teacher->reg_id,
    "first_name" => $teacher->first_name,
    "last_name" => $teacher->last_name,
    "school_reg_id" => $teacher->school_reg_id  

    
);


$_SESSION['mathapp']['login']['sid'] = $data['reg_id'];
 

// pr($data2);
// echo '<a href="section.php?sid='.encrypt_decrypt('encrypt',$data2['school_reg_id']).'">back</a><br>';
// echo '<a href="login.php">next</a>';





require_once DOCUMENT_ROOT . 'system/pages/pupil/v2/header.php';
require_once DOCUMENT_ROOT . 'system/pages/pupil/v2/teacher.php';
require_once DOCUMENT_ROOT . 'system/pages/pupil/v2/footer.php';