<?php

require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php';
require_once DOCUMENT_ROOT.'system/classes/lessons.php';
// $_SESSION['mathapp']['login']['pupil'] = '1';

authorize('pupil');

if( !empty( $_POST ) ){

    // echo 'ANSWERS';
    // echo '<pre>';
    // print_r($_POST);
    // echo '</pre>';


 
    $data = $_POST;

    $add_right_answer=0;
    $sub_right_answer=0;
    $mul_right_answer=0;
    $div_right_answer=0;

    $right_answer=0;
    $wrong_answer=0;
    $unanswered=0;

    $keys=array_keys($data);
    $order=join(",",$keys);

    $query = "select reg_id,answer from tbl_questions where reg_id IN($order) ORDER BY FIELD(reg_id,$order)";

    $database = new Database();
    $db = $database->getConnection();


    $sqlQuery = "SELECT reg_id, answer, number1, number2, operator
            
            FROM tbl_numeracy_questions WHERE  reg_id IN($order) ORDER BY FIELD(reg_id,$order)";

        $stmt = $db->prepare($sqlQuery); 
        $stmt->execute();

        $questionCount = $stmt->rowCount();

        $questionArr = array();
        $questionArr["body"] = array();
        $questionArr["itemCount"] = $questionCount;

        if ($questionCount > 0) { 
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                extract($row);   
                $e = array(
                    "reg_id" => $reg_id,
                    "answer" => $answer,
                    "number1" => $number1,
                    "number2" => $number2,
                    "operator" => $operator 
                );
                
                array_push($questionArr["body"], $e);
            } 
        }

    // echo '<pre>';
    // print_r($questionArr);
    // echo '</pre>';


    // exit;
    $user_id = $_SESSION['mathapp']['login']['pupil'];
    $score_id = $_SESSION['score_id'] = '1';


    $i=0;

    foreach($questionArr['body'] as $result): 

        if($result['answer']==$_POST[$result['reg_id']]){
            $right_answer++; 

            if($result['operator'] == 1){
                $add_right_answer++;
            }
            if($result['operator'] == 2){
                $sub_right_answer++;
            }
            if($result['operator'] == 3){
                $mul_right_answer++;
            }   
            if($result['operator'] == 4){
                $div_right_answer++;
            } 

        }else if($data[$result['reg_id']]=='smart_quiz'){
            $unanswered++;
        }
        else{
            $wrong_answer++;
        }

        $record[$i]['reg_id'] = $result['reg_id'];
        $record[$i]['answer'] = $result['answer'];
        $record[$i]['number1'] = $result['number1'];
        $record[$i]['number2'] = $result['number2'];
        $record[$i]['operator'] = $result['operator'];
        // $record[$i]['option2'] = $result['option2'];
        // $record[$i]['option3'] = $result['option3'];
        // $record[$i]['option4'] = $result['option4'];
        // $record[$i]['answer'] = $result['answer'];
        // $record[$i]['pupil_answer'] = $_POST[$result['reg_id']];

        $i++;

    endforeach;



    
    // echo '<pre>';
    // print_r($record);
    // echo '</pre>';


    $results = array();
    $results['right_answer'] = $right_answer;
    $results['wrong_answer'] = $wrong_answer;
    $results['unanswered'] = $unanswered;


    $results['add_right_answer'] = $add_right_answer;
    $results['sub_right_answer'] = $sub_right_answer;
    $results['mul_right_answer'] = $mul_right_answer;
    $results['div_right_answer'] = $div_right_answer;


    // isset($_SESSION['mathapp']['login']['pupil'] 

    // echo '<pre>';
    // print_r($results);
    // echo '</pre>';

 


        $sqlQuery = "INSERT INTO
                tbl_numeracy_results
            SET
                addition = :addition,
                subtraction = :subtraction,
                multiplication = :multiplication,
                division = :division, 
                right_answer = :right_answer, 
                wrong_answer = :wrong_answer, 
                pupil_id = :pupil_id,
                numeracy = :numeracy ";

        $stmt = $db->prepare($sqlQuery);

        $right_answer=( $results['right_answer']);
        $wrong_answer=( $results['wrong_answer']);

        
        $addition=( $results['add_right_answer']);
        $subtraction=( $results['sub_right_answer']);
        $multiplication=( $results['mul_right_answer']);
        $division=( $results['div_right_answer']);
        
        $pupil_id=$_SESSION['mathapp']['login']['pupil'];

        $numeracy = $_SESSION['mathapp']['quiz']['numeracy'];


 
        $stmt->bindParam(":right_answer", $right_answer);
        $stmt->bindParam(":wrong_answer", $wrong_answer);

        $stmt->bindParam(":addition", $addition);
        $stmt->bindParam(":subtraction", $subtraction);
        $stmt->bindParam(":multiplication", $multiplication);
        $stmt->bindParam(":division", $division);
        
        $stmt->bindParam(":pupil_id", $pupil_id);
        $stmt->bindParam(":numeracy", $numeracy);
        

    if ($stmt->execute()) {
        // echo 'OK';
    } else {
        // echo json_encode($stmt->errorInfo(),true);exit;
    }
 
    /*
    $right_answer=0;
    $wrong_answer=0;
    $unanswered=0;
    $keys=array_keys($data);
    $order=join(",",$keys);
    $query = "select id,answer from questions where id IN($order) ORDER BY FIELD(id,$order)";
    $response=mysqli_query( $_con, $query)   or die(mysqli_error());
    
    $user_id = $_SESSION['id'];
    $score_id = $_SESSION['score_id'];
    while($result=mysqli_fetch_array($response)){
        if($result['answer']==$_POST[$result['id']]){
            $right_answer++;
        }else if($data[$result['id']]=='smart_quiz'){
            $unanswered++;
        }
        else{
            $wrong_answer++;
        }
    }
    $results = array();
    $results['right_answer'] = $right_answer;
    $results['wrong_answer'] = $wrong_answer;
    $results['unanswered'] = $unanswered;

    $update_query = "update scores set right_answer='$right_answer', wrong_answer = '$wrong_answer', unanswered = '$unanswered' where user_id='$user_id' and id ='$score_id' ";
    mysqli_query( $_con, $update_query)   or die(mysqli_error());
    mysqli_close($_con);
    return $results;*/
}	


// echo '<br><br><a href="index.php">BACK</a>';


 

// $database = new Database();
// $db = $database->getConnection();

// $lesson = new Lessons($db);
// $lesson->reg_id = $lesson_id;  

// $lesson->getSingle();




require_once DOCUMENT_ROOT . 'system/pages/pupil/new/header.php'; 
require_once DOCUMENT_ROOT . 'system/pages/pupil/new/numeracy_result.php'; 
require_once DOCUMENT_ROOT . 'system/pages/pupil/new/footer.php'; 