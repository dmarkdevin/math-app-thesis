<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/pupils.php'; 
require_once DOCUMENT_ROOT.'system/classes/category.php'; 
require_once DOCUMENT_ROOT.'system/classes/modules.php'; 
require_once DOCUMENT_ROOT.'system/classes/lessons.php'; 
require_once DOCUMENT_ROOT.'system/classes/numeracyQuestions.php'; 
require_once DOCUMENT_ROOT.'system/classes/pupils.php'; 


authorize('pupil');

$label = 'quiz';
$main_page = 'index.php'; 


if(empty($_GET['n'])){
    header('location: '.$main_page);
    exit;
} 

$database = new Database();
$db = $database->getConnection(); 

$question = new numeracyQuestions($db);

$a =array();
$s =array();
$m =array();
$d = array();

$_SESSION['mathapp']['quiz']['numeracy'] = $_GET['n'];




$pupil = new Pupils($db);
$pupil->reg_id = $_SESSION['mathapp']['login']['pupil'];  

$pupil->getSingle();

if($pupil->reg_id == null){
    header('location: '.$main_page);
    exit;
}





$question->operator = 1;
$question->teacher_reg_id = $pupil->teacher_reg_id; //$_SESSION['mathapp']['login']['teacher']; 

$stmt = $question->getByOperator();
$data1Count = $stmt->rowCount();


$numerayQuestions = array();

$dataArray = array();
$dataArray["body"] = array();
$dataArray["itemCount"] = $data1Count;

if ($data1Count > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "number1" => $number1,
            "number2" => $number2,
            "operator" => $operator,
            "answer" => $answer 
        );
        
        array_push($dataArray["body"], $e);
    }

    $a = $dataArray["body"];
}

// pr($dataArray1);


$question->operator = 2;  
 

$stmt = $question->getByOperator();
$dataCount = $stmt->rowCount();

$dataArray = array();
$dataArray["body"] = array();
$dataArray["itemCount"] = $dataCount;

if ($dataCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "number1" => $number1,
            "number2" => $number2,
            "operator" => $operator,
            "answer" => $answer 
        );
        
        array_push($dataArray["body"], $e);
         
    } 
    $s = $dataArray["body"];
}

 
$question->operator = 3;  
 

$stmt = $question->getByOperator();
$dataCount = $stmt->rowCount();

$dataArray = array();
$dataArray["body"] = array();
$dataArray["itemCount"] = $dataCount;

if ($dataCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "number1" => $number1,
            "number2" => $number2,
            "operator" => $operator,
            "answer" => $answer 
        );
        
        array_push($dataArray["body"], $e);
    } 

    $m = $dataArray["body"];
}

// pr($dataArray);



$question->operator = 4;  
 

$stmt = $question->getByOperator();
$dataCount = $stmt->rowCount();

$dataArray = array();
$dataArray["body"] = array();
$dataArray["itemCount"] = $dataCount;

if ($dataCount > 0) { 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);   
        $e = array(
            "reg_id" => $reg_id,
            "number1" => $number1,
            "number2" => $number2,
            "operator" => $operator,
            "answer" => $answer 
        );
        
        array_push($dataArray["body"], $e);
    } 

    $d = $dataArray["body"];
}

// pr($dataArray);

$dataArray["body"] = array_merge($a, $s, $m, $d);
$dataCount = count($dataArray["body"]);

$results = array();
$number_question = 1;

$rowcount = $dataCount;

$remainder = $rowcount/$number_question;
$results['number_question'] = $number_question;
$results['remainder'] = $remainder;
$results['rowcount'] = $rowcount;

$results['questions'] = array();

foreach ( $dataArray["body"] as $result ) {
    $results['questions'][] = $result;
}



// pr($results);


require_once DOCUMENT_ROOT.'system/pages/pupil/numeracy_quiz.php';






