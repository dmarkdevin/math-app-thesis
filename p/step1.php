<?php
require_once '../global.php';
require_once DOCUMENT_ROOT.'system/config.php';
require_once DOCUMENT_ROOT.'system/classes/database.php'; 
require_once DOCUMENT_ROOT.'system/classes/pupils.php'; 
require_once DOCUMENT_ROOT.'system/classes/category.php'; 
require_once DOCUMENT_ROOT.'system/classes/modules.php'; 

authorize('pupil');

$label = 'step1';
$main_page = 'index.php';

// if(empty($_GET['c'])){
//     header('location:'.$main_page);
//     exit;
// }

// $decryptedID = encrypt_decrypt('decrypt', $_GET['c']);

// $database = new Database();
// $db = $database->getConnection();

// $category = new Category($db);
// $category->id = $decryptedID;  

// $category->getSingle();

// if($category->id == null){
//     header('location: '.$main_page);
//     exit;
// }

$database = new Database();
$db = $database->getConnection();

$pupil = new Pupils($db);
$pupil->reg_id = $_SESSION['mathapp']['login']['pupil'];  

$pupil->getClass();

if($pupil->reg_id == null){
    header('location: '.$main_page);
    exit;
}
 

require_once DOCUMENT_ROOT . 'system/pages/pupil/new/header.php'; 
require_once DOCUMENT_ROOT . 'system/pages/pupil/new/step1.php'; 
require_once DOCUMENT_ROOT . 'system/pages/pupil/new/footer.php'; 